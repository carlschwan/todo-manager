// Copyright 2019 Alain M.(https://github.com/alainm23/planner)
// Copyright 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#include "collaborator.h"

Collaborator::Collaborator(QObject *parent)
    : QObject(parent)
{
}

Collaborator::~Collaborator()
{
}

QString Collaborator::imageId() const
{
    return m_imageId;
}

void Collaborator::setImageId(const QString& imageId)
{
    if (m_imageId == imageId) {
        return;
    }

    m_imageId = imageId;
    emit imageIdChanged(m_imageId);
}

QTimeZone Collaborator::timezone() const
{
    return m_timezone;
}

void Collaborator::setTimezone(const QTimeZone& timezone)
{
    if (m_timezone == timezone) {
        return;
    }

    m_timezone = timezone;
    emit timezoneChanged(m_timezone);
}

QString Collaborator::fullName() const
{
    return m_fullName;
}

void Collaborator::setFullName(const QString& fullName)
{
    if (m_fullName == fullName) {
        return;
    }

    m_fullName = fullName;
    emit fullNameChanged(m_fullName);
}

QString Collaborator::email() const
{
    return m_email;
}

void Collaborator::setEmail(const QString& email)
{
    if (m_email == email) {
        return;
    }

    m_email = email;
    emit emailChanged(m_email);
}

qint64 Collaborator::id() const
{
    return m_id;
}

void Collaborator::setId(qint64 id)
{
    if (m_id == id) {
        return;
    }

    m_id = id;
    emit idChanged(m_id);
}
