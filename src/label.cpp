// Copyright 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#include "label.h"

Label::Label(QObject *parent)
    : QObject(parent)
{
}

Label::~Label()
{
}

bool Label::isTodoist() const
{
    return m_isTodoist;
}

void Label::setIsTodoist(bool isTodoist)
{
    if (m_isTodoist == isTodoist) {
        return;
    }

    m_isTodoist = isTodoist;
    emit isTodoistChanged(m_isTodoist);
}

bool Label::isFavorite() const
{
    return m_isFavorite;
}

void Label::setIsFavorite(bool isFavorite)
{
    if (m_isFavorite == isFavorite) {
        return;
    }

    m_isFavorite = isFavorite;
    emit isFavoriteChanged(m_isFavorite);
}

bool Label::isDeleted() const
{
    return m_isDeleted;
}

void Label::setIsDeleted(bool isDeleted)
{
    if (m_isDeleted == isDeleted) {
        return;
    }

    m_isDeleted = isDeleted;
    emit isDeletedChanged(m_isDeleted);
}

int Label::itemOrder() const
{
    return m_itemOrder;
}

void Label::setItemOrder(int itemOrder)
{
    if (m_itemOrder == itemOrder) {
        return;
    }

    m_itemOrder = itemOrder;
    emit itemOrderChanged(m_itemOrder);
}

int Label::color() const
{
    return m_color;
}

void Label::setColor(int color)
{
    if (m_color == color) {
        return;
    }

    m_color = color;
    emit colorChanged(m_color);
}

QString Label::name() const
{
    return m_name;
}

void Label::setName(const QString& name)
{
    if (m_name == name) {
        return;
    }

    m_name = name;
    emit nameChanged(m_name);
}

qint64 Label::projectId() const
{
    return m_projectId;
}

void Label::setProjectId(qint64 projectId)
{
    if (m_projectId == projectId) {
        return;
    }

    m_projectId = projectId;
    emit projectIdChanged(m_projectId);
}

qint64 Label::itemLabelId() const
{
    return m_itemLabelId;
}

void Label::setItemLabelId(qint64 itemLabelId)
{
    if (m_itemLabelId == itemLabelId) {
        return;
    }

    m_itemLabelId = itemLabelId;
    emit itemLabelIdChanged(m_itemLabelId);
}

qint64 Label::id() const
{
    return m_id;
}

void Label::setId(qint64 id)
{
    if (m_id == id) {
        return;
    }

    m_id = id;
    emit idChanged(m_id);
}
