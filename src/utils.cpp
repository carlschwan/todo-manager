// Copyright 2019 Alain M.(https://github.com/alainm23/planner)
// Copyright 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#include "utils.h"
#include <QRandomGenerator>
#include <QtMath>

const static QString NUMERIC_CHARS = QStringLiteral("0123456789");

Utils::Utils(QObject *parent)
    : QObject(parent)
{
}

qint64 Utils::generateId(int len)
{
    QRandomGenerator generator;
    return generator.bounded(0ll, static_cast<qint32>(qPow(10, len)));
}
