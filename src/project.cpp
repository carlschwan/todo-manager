// SPDX-FileCopyrightText: 2019 Alain M.(https://github.com/alainm23/planner)
// SPDX-FileCopyrightText: 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#include "project.h"

Project::Project(QObject *parent)
    : QObject(parent)
{
}

Project::~Project()
{
}

bool Project::collapsed() const
{
    return m_collapsed;
}

void Project::setCollapsed(bool collapsed)
{
    if (m_collapsed == collapsed) {
        return;
    }

    m_collapsed = collapsed;
    emit collapsedChanged(m_collapsed);
}

int Project::sortOrder() const
{
    return m_sortOrder;
}

void Project::setSortOrder(int sortOrder)
{
    if (m_sortOrder == sortOrder) {
        return;
    }

    m_sortOrder = sortOrder;
    emit sortOrderChanged(m_sortOrder);
}

bool Project::showCompleted() const
{
    return m_showCompleted;
}

void Project::setShowCompleted(bool showCompleted)
{
    if (m_showCompleted == showCompleted) {
        return;
    }

    m_showCompleted = showCompleted;
    emit showCompletedChanged(m_showCompleted);
}

bool Project::isKanban() const
{
    return m_isKanban;
}

void Project::setIsKanban(bool isKanban)
{
    if (m_isKanban == isKanban) {
        return;
    }

    m_isKanban = isKanban;
    emit isKanbanChanged(m_isKanban);
}

bool Project::shared() const
{
    return m_shared;
}

void Project::setShared(bool shared)
{
    if (m_shared == shared) {
        return;
    }

    m_shared = shared;
    emit sharedChanged(m_shared);
}

bool Project::isSync() const
{
    return m_isSync;
}

void Project::setIsSync(bool isSync)
{
    if (m_isSync == isSync) {
        return;
    }

    m_isSync = isSync;
    emit isSyncChanged(m_isSync);
}

bool Project::isFavorite() const
{
    return m_isFavorite;
}

void Project::setIsFavorite(bool isFavorite)
{
    if (m_isFavorite == isFavorite) {
        return;
    }

    m_isFavorite = isFavorite;
    emit isFavoriteChanged(m_isFavorite);
}

bool Project::isArchived() const
{
    return m_isArchived;
}

void Project::setIsArchived(bool isArchived)
{
    if (m_isArchived == isArchived) {
        return;
    }

    m_isArchived = isArchived;
    emit isArchivedChanged(m_isArchived);
}

bool Project::isDeleted() const
{
    return m_isDeleted;
}

void Project::setIsDeleted(bool isDeleted)
{
    if (m_isDeleted == isDeleted) {
        return;
    }

    m_isDeleted = isDeleted;
    emit isDeletedChanged(m_isDeleted);
}

int Project::itemOrder() const
{
    return m_itemOrder;
}

void Project::setItemOrder(int itemOrder)
{
    if (m_itemOrder == itemOrder) {
        return;
    }

    m_itemOrder = itemOrder;
    emit itemOrderChanged(m_itemOrder);
}

bool Project::isTeamInbox() const
{
    return m_isTeamInbox;
}

void Project::setIsTeamInbox(bool isTeamInbox)
{
    if (m_isTeamInbox == isTeamInbox) {
        return;
    }

    m_isTeamInbox = isTeamInbox;
    emit isTeamInboxChanged(m_isTeamInbox);
}

bool Project::inboxProject() const
{
    return m_inboxProject;
}

void Project::setInboxProject(bool inboxProject)
{
    if (m_inboxProject == inboxProject) {
        return;
    }

    m_inboxProject = inboxProject;
    emit inboxProjectChanged(m_inboxProject);
}

bool Project::isTodoist() const
{
    return m_isTodoist;
}

void Project::setIsTodoist(bool isTodoist)
{
    if (m_isTodoist == isTodoist) {
        return;
    }

    m_isTodoist = isTodoist;
    emit isTodoistChanged(m_isTodoist);
}

int Project::color() const
{
    return m_color;
}

void Project::setColor(int color)
{
    if (m_color == color) {
        return;
    }

    m_color = color;
    emit colorChanged(m_color);
}

QDateTime Project::dueDate() const
{
    return m_dueDate;
}

void Project::setDueDate(const QDateTime& dueDate)
{
    if (m_dueDate == dueDate) {
        return;
    }

    m_dueDate = dueDate;
    emit dueDateChanged(m_dueDate);
}

QString Project::note() const
{
    return m_note;
}

void Project::setNote(const QString& note)
{
    if (m_note == note) {
        return;
    }

    m_note = note;
    emit noteChanged(m_note);
}

QString Project::name() const
{
    return m_name;
}

void Project::setName(const QString& name)
{
    if (m_name == name) {
        return;
    }

    m_name = name;
    emit nameChanged(m_name);
}

qint64 Project::id() const
{
    return m_id;
}

void Project::setId(qint64 id)
{
    if (m_id == id) {
        return;
    }

    m_id = id;
    emit idChanged(m_id);
}

qint64 Project::parentId() const
{
    return m_parentId;
}

void Project::setParentId(qint64 parentId)
{
    if (m_parentId == parentId) {
        return;
    }

    m_parentId = parentId;
    emit parentIdChanged(m_parentId);
}

qint64 Project::areaId() const
{
    return m_areaId;
}

void Project::setAreaId(qint64 areaId)
{
    if (m_areaId == areaId) {
        return;
    }

    m_areaId = areaId;
    emit areaIdChanged(m_areaId);
}
