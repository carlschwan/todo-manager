// Copyright 2019 Alain M.(https://github.com/alainm23/planner)
// Copyright 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#pragma once

#include <QObject>

/**
 * General utilities for TodoManager.
 */
class Utils : public QObject
{
    Q_OBJECT

public:
    Utils(QObject *parent = nullptr);

    qint64 generateId(int len = 10);
};
