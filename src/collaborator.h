// Copyright 2019 Alain M.(https://github.com/alainm23/planner)
// Copyright 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#pragma once

#include <QObject>
#include <QTimeZone>

/**
 * @todo write docs
 */
class Collaborator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString imageId READ imageId WRITE setImageId NOTIFY imageIdChanged)
    Q_PROPERTY(QString timezone READ timezone WRITE setTimezone NOTIFY timezoneChanged)
    Q_PROPERTY(QTimeZone fullName READ fullName WRITE setFullName NOTIFY fullNameChanged)
    Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY emailChanged)
    Q_PROPERTY(qint64 id READ id WRITE setId NOTIFY idChanged)

public:
    /**
     * Default constructor
     */
    Collaborator(QObject *parent = nullptr);

    /**
     * Destructor
     */
    ~Collaborator();

    /**
     * @return the imageId
     */
    QString imageId() const;

    /**
     * @return the timezone
     */
    QTimeZone timezone() const;

    /**
     * @return the fullName
     */
    QString fullName() const;

    /**
     * @return the email
     */
    QString email() const;

    /**
     * @return the id
     */
    qint64 id() const;

public Q_SLOTS:
    /**
     * Sets the imageId.
     *
     * @param imageId the new imageId
     */
    void setImageId(const QString& imageId);

    /**
     * Sets the timezone.
     *
     * @param timezone the new timezone
     */
    void setTimezone(const QTimeZone &timezone);

    /**
     * Sets the fullName.
     *
     * @param fullName the new fullName
     */
    void setFullName(const QString& fullName);

    /**
     * Sets the email.
     *
     * @param email the new email
     */
    void setEmail(const QString& email);

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    void setId(qint64 id);

Q_SIGNALS:
    void imageIdChanged(const QString& imageId);

    void timezoneChanged(const QTimeZone& timezone);

    void fullNameChanged(const QString& fullName);

    void emailChanged(const QString& email);

    void idChanged(qint64 id);

private:
    QString m_imageId;
    QTimeZone m_timezone;
    QString m_fullName;
    QString m_email;
    qint64 m_id;
};
