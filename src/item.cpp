// Copyright 2019 Alain M.(https://github.com/alainm23/planner)
// Copyright 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#include "item.h"

Item::Item(QObject *parent)
    : QObject(parent)
{
}

Item::~Item()
{
}

QDateTime Item::dateUpdated() const
{
    return m_dateUpdated;
}

void Item::setDateUpdated(const QDateTime& dateUpdated)
{
    if (m_dateUpdated == dateUpdated) {
        return;
    }

    m_dateUpdated = dateUpdated;
    emit dateUpdatedChanged(m_dateUpdated);
}

QDateTime Item::dateCompleted() const
{
    return m_dateCompleted;
}

void Item::setDateCompleted(const QDateTime& dateCompleted)
{
    if (m_dateCompleted == dateCompleted) {
        return;
    }

    m_dateCompleted = dateCompleted;
    emit dateCompletedChanged(m_dateCompleted);
}

QDateTime Item::dateAdded() const
{
    return m_dateAdded;
}

void Item::setDateAdded(const QDateTime& dateAdded)
{
    if (m_dateAdded == dateAdded) {
        return;
    }

    m_dateAdded = dateAdded;
    emit dateAddedChanged(m_dateAdded);
}

int Item::collapsed() const
{
    return m_collapsed;
}

void Item::setCollapsed(int collapsed)
{
    if (m_collapsed == collapsed) {
        return;
    }

    m_collapsed = collapsed;
    emit collapsedChanged(m_collapsed);
}

bool Item::dueIsRecurring() const
{
    return m_dueIsRecurring;
}

void Item::setDueIsRecurring(bool dueIsRecurring)
{
    if (m_dueIsRecurring == dueIsRecurring) {
        return;
    }

    m_dueIsRecurring = dueIsRecurring;
    emit dueIsRecurringChanged(m_dueIsRecurring);
}

QString Item::dueLang() const
{
    return m_dueLang;
}

void Item::setDueLang(const QString& dueLang)
{
    if (m_dueLang == dueLang) {
        return;
    }

    m_dueLang = dueLang;
    emit dueLangChanged(m_dueLang);
}

QString Item::dueString() const
{
    return m_dueString;
}

void Item::setDueString(const QString& dueString)
{
    if (m_dueString == dueString) {
        return;
    }

    m_dueString = dueString;
    emit dueStringChanged(m_dueString);
}

QTimeZone Item::dueTimezone() const
{
    return m_dueTimezone;
}

void Item::setDueTimezone(const QTimeZone& dueTimezone)
{
    if (m_dueTimezone == dueTimezone) {
        return;
    }

    m_dueTimezone = dueTimezone;
    emit dueTimezoneChanged(m_dueTimezone);
}

QDate Item::dueDate() const
{
    return m_dueDate;
}

void Item::setDueDate(const QDate& dueDate)
{
    if (m_dueDate == dueDate) {
        return;
    }

    m_dueDate = dueDate;
    emit dueDateChanged(m_dueDate);
}

QString Item::content() const
{
    return m_content;
}

void Item::setContent(const QString& content)
{
    if (m_content == content) {
        return;
    }

    m_content = content;
    emit contentChanged(m_content);
}

bool Item::isTodoist() const
{
    return m_isTodoist;
}

void Item::setIsTodoist(bool isTodoist)
{
    if (m_isTodoist == isTodoist) {
        return;
    }

    m_isTodoist = isTodoist;
    emit isTodoistChanged(m_isTodoist);
}

bool Item::isDeleted() const
{
    return m_isDeleted;
}

void Item::setIsDeleted(bool isDeleted)
{
    if (m_isDeleted == isDeleted) {
        return;
    }

    m_isDeleted = isDeleted;
    emit isDeletedChanged(m_isDeleted);
}

int Item::checked() const
{
    return m_checked;
}

void Item::setChecked(int checked)
{
    if (m_checked == checked) {
        return;
    }

    m_checked = checked;
    emit checkedChanged(m_checked);
}

int Item::dayOrder() const
{
    return m_dayOrder;
}

void Item::setDayOrder(int dayOrder)
{
    if (m_dayOrder == dayOrder) {
        return;
    }

    m_dayOrder = dayOrder;
    emit dayOrderChanged(m_dayOrder);
}

int Item::itemOrder() const
{
    return m_itemOrder;
}

void Item::setItemOrder(int itemOrder)
{
    if (m_itemOrder == itemOrder) {
        return;
    }

    m_itemOrder = itemOrder;
    emit itemOrderChanged(m_itemOrder);
}

int Item::priority() const
{
    return m_priority;
}

void Item::setPriority(int priority)
{
    if (m_priority == priority) {
        return;
    }

    m_priority = priority;
    emit priorityChanged(m_priority);
}

int Item::parentId() const
{
    return m_parentId;
}

void Item::setParentId(int parentId)
{
    if (m_parentId == parentId) {
        return;
    }

    m_parentId = parentId;
    emit parentIdChanged(m_parentId);
}

int Item::syncId() const
{
    return m_syncId;
}

void Item::setSyncId(int syncId)
{
    if (m_syncId == syncId) {
        return;
    }

    m_syncId = syncId;
    emit syncIdChanged(m_syncId);
}

int Item::responsibleUid() const
{
    return m_responsibleUid;
}

void Item::setResponsibleUid(int responsibleUid)
{
    if (m_responsibleUid == responsibleUid) {
        return;
    }

    m_responsibleUid = responsibleUid;
    emit responsibleUidChanged(m_responsibleUid);
}

int Item::assignedByUid() const
{
    return m_assignedByUid;
}

void Item::setAssignedByUid(int assignedByUid)
{
    if (m_assignedByUid == assignedByUid) {
        return;
    }

    m_assignedByUid = assignedByUid;
    emit assignedByUidChanged(m_assignedByUid);
}

int Item::userId() const
{
    return m_userId;
}

void Item::setUserId(int userId)
{
    if (m_userId == userId) {
        return;
    }

    m_userId = userId;
    emit userIdChanged(m_userId);
}

int Item::sectionId() const
{
    return m_sectionId;
}

void Item::setSectionId(int sectionId)
{
    if (m_sectionId == sectionId) {
        return;
    }

    m_sectionId = sectionId;
    emit sectionIdChanged(m_sectionId);
}

int Item::projectId() const
{
    return m_projectId;
}

void Item::setProjectId(int projectId)
{
    if (m_projectId == projectId) {
        return;
    }

    m_projectId = projectId;
    emit projectIdChanged(m_projectId);
}

int Item::id() const
{
    return m_id;
}

void Item::setId(int id)
{
    if (m_id == id) {
        return;
    }

    m_id = id;
    emit idChanged(m_id);
}
