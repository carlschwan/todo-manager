// Copyright 2019 Alain M.(https://github.com/alainm23/planner)
// Copyright 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#pragma once

#include <QDateTime>
#include <QTimeZone>
#include <QObject>

/**
 * An simple todo item.
 */
class Item : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDateTime dateUpdated READ dateUpdated WRITE setDateUpdated NOTIFY dateUpdatedChanged)
    Q_PROPERTY(QDateTime dateCompleted READ dateCompleted WRITE setDateCompleted NOTIFY dateCompletedChanged)
    Q_PROPERTY(QDateTime dateAdded READ dateAdded WRITE setDateAdded NOTIFY dateAddedChanged)
    Q_PROPERTY(int collapsed READ collapsed WRITE setCollapsed NOTIFY collapsedChanged)
    Q_PROPERTY(bool dueIsRecurring READ dueIsRecurring WRITE setDueIsRecurring NOTIFY dueIsRecurringChanged)
    Q_PROPERTY(QString dueLang READ dueLang WRITE setDueLang NOTIFY dueLangChanged)
    Q_PROPERTY(QString dueString READ dueString WRITE setDueString NOTIFY dueStringChanged)
    Q_PROPERTY(QTimeZone dueTimezone READ dueTimezone WRITE setDueTimezone NOTIFY dueTimezoneChanged)
    Q_PROPERTY(QDate dueDate READ dueDate WRITE setDueDate NOTIFY dueDateChanged)
    Q_PROPERTY(QString content READ content WRITE setContent NOTIFY contentChanged)
    Q_PROPERTY(bool isTodoist READ isTodoist WRITE setIsTodoist NOTIFY isTodoistChanged)
    Q_PROPERTY(bool isDeleted READ isDeleted WRITE setIsDeleted NOTIFY isDeletedChanged)
    Q_PROPERTY(int checked READ checked WRITE setChecked NOTIFY checkedChanged)
    Q_PROPERTY(int dayOrder READ dayOrder WRITE setDayOrder NOTIFY dayOrderChanged)
    Q_PROPERTY(int itemOrder READ itemOrder WRITE setItemOrder NOTIFY itemOrderChanged)
    Q_PROPERTY(int priority READ priority WRITE setPriority NOTIFY priorityChanged)
    Q_PROPERTY(int parentId READ parentId WRITE setParentId NOTIFY parentIdChanged)
    Q_PROPERTY(int syncId READ syncId WRITE setSyncId NOTIFY syncIdChanged)
    Q_PROPERTY(int responsibleUid READ responsibleUid WRITE setResponsibleUid NOTIFY responsibleUidChanged)
    Q_PROPERTY(int assignedByUid READ assignedByUid WRITE setAssignedByUid NOTIFY assignedByUidChanged)
    Q_PROPERTY(int userId READ userId WRITE setUserId NOTIFY userIdChanged)
    Q_PROPERTY(int sectionId READ sectionId WRITE setSectionId NOTIFY sectionIdChanged)
    Q_PROPERTY(int projectId READ projectId WRITE setProjectId NOTIFY projectIdChanged)
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)

public:
    Item(QObject *parent = nullptr);
    ~Item();

    /**
     * @return the dateUpdated
     */
    QDateTime dateUpdated() const;

    /**
     * @return the dateCompleted
     */
    QDateTime dateCompleted() const;

    /**
     * @return the dateAdded
     */
    QDateTime dateAdded() const;

    /**
     * @return the collapsed
     */
    int collapsed() const;

    /**
     * @return the dueIsRecurring
     */
    bool dueIsRecurring() const;

    /**
     * @return the dueLang
     */
    QString dueLang() const;

    /**
     * @return the dueString
     */
    QString dueString() const;

    /**
     * @return the dueTimezone
     */
    QTimeZone dueTimezone() const;

    /**
     * @return the dueDate
     */
    QDate dueDate() const;

    /**
     * @return the content
     */
    QString content() const;

    /**
     * @return the isTodoist
     */
    bool isTodoist() const;

    /**
     * @return the isDeleted
     */
    bool isDeleted() const;

    /**
     * @return the checked
     */
    int checked() const;

    /**
     * @return the dayOrder
     */
    int dayOrder() const;

    /**
     * @return the itemOrder
     */
    int itemOrder() const;

    /**
     * @return the priority
     */
    int priority() const;

    /**
     * @return the parentId
     */
    int parentId() const;

    /**
     * @return the syncId
     */
    int syncId() const;

    /**
     * @return the responsibleUid
     */
    int responsibleUid() const;

    /**
     * @return the assignedByUid
     */
    int assignedByUid() const;

    /**
     * @return the userId
     */
    int userId() const;

    /**
     * @return the sectionId
     */
    int sectionId() const;

    /**
     * @return the projectId
     */
    int projectId() const;

    /**
     * @return the id
     */
    int id() const;

public Q_SLOTS:
    /**
     * Sets the dateUpdated.
     *
     * @param dateUpdated the new dateUpdated
     */
    void setDateUpdated(const QDateTime& dateUpdated);

    /**
     * Sets the dateCompleted.
     *
     * @param dateCompleted the new dateCompleted
     */
    void setDateCompleted(const QDateTime& dateCompleted);

    /**
     * Sets the dateAdded.
     *
     * @param dateAdded the new dateAdded
     */
    void setDateAdded(const QDateTime& dateAdded);

    /**
     * Sets the collapsed.
     *
     * @param collapsed the new collapsed
     */
    void setCollapsed(int collapsed);

    /**
     * Sets the dueIsRecurring.
     *
     * @param dueIsRecurring the new dueIsRecurring
     */
    void setDueIsRecurring(bool dueIsRecurring);

    /**
     * Sets the dueLang.
     *
     * @param dueLang the new dueLang
     */
    void setDueLang(const QString& dueLang);

    /**
     * Sets the dueString.
     *
     * @param dueString the new dueString
     */
    void setDueString(const QString& dueString);

    /**
     * Sets the dueTimezone.
     *
     * @param dueTimezone the new dueTimezone
     */
    void setDueTimezone(const QTimeZone& dueTimezone);

    /**
     * Sets the dueDate.
     *
     * @param dueDate the new dueDate
     */
    void setDueDate(const QDate& dueDate);

    /**
     * Sets the content.
     *
     * @param content the new content
     */
    void setContent(const QString& content);

    /**
     * Sets the isTodoist.
     *
     * @param isTodoist the new isTodoist
     */
    void setIsTodoist(bool isTodoist);

    /**
     * Sets the isDeleted.
     *
     * @param isDeleted the new isDeleted
     */
    void setIsDeleted(bool isDeleted);

    /**
     * Sets the checked.
     *
     * @param checked the new checked
     */
    void setChecked(int checked);

    /**
     * Sets the dayOrder.
     *
     * @param dayOrder the new dayOrder
     */
    void setDayOrder(int dayOrder);

    /**
     * Sets the itemOrder.
     *
     * @param itemOrder the new itemOrder
     */
    void setItemOrder(int itemOrder);

    /**
     * Sets the priority.
     *
     * @param priority the new priority
     */
    void setPriority(int priority);

    /**
     * Sets the parentId.
     *
     * @param parentId the new parentId
     */
    void setParentId(int parentId);

    /**
     * Sets the syncId.
     *
     * @param syncId the new syncId
     */
    void setSyncId(int syncId);

    /**
     * Sets the responsibleUid.
     *
     * @param responsibleUid the new responsibleUid
     */
    void setResponsibleUid(int responsibleUid);

    /**
     * Sets the assignedByUid.
     *
     * @param assignedByUid the new assignedByUid
     */
    void setAssignedByUid(int assignedByUid);

    /**
     * Sets the userId.
     *
     * @param userId the new userId
     */
    void setUserId(int userId);

    /**
     * Sets the sectionId.
     *
     * @param sectionId the new sectionId
     */
    void setSectionId(int sectionId);

    /**
     * Sets the projectId.
     *
     * @param projectId the new projectId
     */
    void setProjectId(int projectId);

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    void setId(int id);

Q_SIGNALS:
    void dateUpdatedChanged(const QDateTime& dateUpdated);

    void dateCompletedChanged(const QDateTime& dateCompleted);

    void dateAddedChanged(const QDateTime& dateAdded);

    void collapsedChanged(int collapsed);

    void dueIsRecurringChanged(bool dueIsRecurring);

    void dueLangChanged(const QString& dueLang);

    void dueStringChanged(const QString& dueString);

    void dueTimezoneChanged(const QTimeZone& dueTimezone);

    void dueDateChanged(const QDate& dueDate);

    void contentChanged(const QString& content);

    void isTodoistChanged(bool isTodoist);

    void isDeletedChanged(bool isDeleted);

    void checkedChanged(int checked);

    void dayOrderChanged(int dayOrder);

    void itemOrderChanged(int itemOrder);

    void priorityChanged(int priority);

    void parentIdChanged(int parentId);

    void syncIdChanged(int syncId);

    void responsibleUidChanged(int responsibleUid);

    void assignedByUidChanged(int assignedByUid);

    void userIdChanged(int userId);

    void sectionIdChanged(int sectionId);

    void projectIdChanged(int projectId);

    void idChanged(int id);

private:
    QDateTime m_dateUpdated;
    QDateTime m_dateCompleted;
    QDateTime m_dateAdded;
    int m_collapsed;
    bool m_dueIsRecurring;
    QString m_dueLang;
    QString m_dueString;
    QTimeZone m_dueTimezone;
    QDate m_dueDate;
    QString m_content;
    bool m_isTodoist;
    bool m_isDeleted;
    int m_checked;
    int m_dayOrder;
    int m_itemOrder;
    int m_priority;
    int m_parentId;
    int m_syncId;
    int m_responsibleUid;
    int m_assignedByUid;
    int m_userId;
    int m_sectionId;
    int m_projectId;
    int m_id;
};
