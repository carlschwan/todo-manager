// Copyright 2019 Alain M.(https://github.com/alainm23/planner)
// Copyright 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#pragma once

#include <QObject>
#include <QSqlDatabase>

class Project;
class Section;
class Item;
class Reminder;
class Label;
class Queue;
class Collaborator;

class Database : public QObject
{
    Q_OBJECT

public:
    Database(QObject *parent = nullptr);
    ~Database();
    
    void openDatabase();
    QString getDatabasePath();
    void patchDatabase();
    bool createDatabase();
    bool isDatabaseEmpty() const;
    void resetAll();
    
    void insertQuickFindRecents(QString type, QString object);
    QStringList getQuickFindRecents();
    
    void addItemToDelete(Item *item);
    void removeItemToDelete();
    
    bool projectExist(qint64 project);
    
    // Queue
    bool insertQueue(const Queue &queue) const;
    Queue &&getQueueByObjectId(qint64 id) const;
    QList<Queue *> &&getAllQueue() const;
    void updateQueue(const Queue &queue) const;
    void removeQueue(const QString &uuid) const;
    void clearQueue() const;
    
    bool insertCurTempIds(qint64 id, QString tempId, QString object);
    void removeCurTempIds(qint64 id);
    
    void clearCurTempIds ();
    bool curTempIdsExists(qint64 id);
    QString getTempId(qint64 id);
    
    // Colaborator
    bool insertCollaborator(const Collaborator &collaborator);
    
    // Projects
    Project *createInboxProject() const;
    int getProjectCountByArea(qint64 id) const;
    bool insertProject(Project *project);
    void updateProject(Project *project);
    void updateSortOrderProject(qint64 projectId, int orden);
    void updateItemId(qint64 currentId, qint64 newId);
    void updateSectionId(qint64 currentId, qint64 newId);
    void updateProjectId(qint64 currentId, qint64 newId);
    void deleteProject(qint64 id);
    bool moveProject(Project *project, qint64 parent_id);
    /// Allocate memory that the caller needs to free.
    QList<Project *> getAllProjectsByParent(qint64 id) const;
Q_SIGNALS:
    
    void updateAllBage();
    void projectAdded(Project *project);
    void projectUpdated(Project *project);
    void projectDeleted(qint64 id);
    void projectShow_completed(Project *project);
    void projectMoved(Project *project, qint64 parentId, qint64 oldParentId);
    void updateProjectCount(qint64 id, int items0, int items1);
    void projectIdUpdated(qint64 currentId, qint64 newDd);

    void subtractTaskCounter(qint64 id);

    void sectionAdded(Section *section, int index);
    void sectionDeleted(Section *section);
    void sectionUpdated(Section *section);
    void sectionMoved(Section *section, qint64 projectId, qint64 oldProjectId);
    void sectionIdUpdated(qint64 currentId, qint64 newId);

    void itemAdded(Item *item, int index);
    void itemAddedWithIndex(Item *item, int index);
    void itemUpdated(Item *item);
    void itemDeleted(Item *item);
    //void on_drag_item_deleted(Widgets.ItemRow row, qint64 section_id);
    void addDueItem(Item *item, int index);
    void updateDueItem(Item *item, int index);
    void removeDueItem(Item *item);
    void itemLabelAdded(qint64 id, qint64 itemId, Label label);
    void itemLabelDeleted(qint64 id, qint64 itemId, Label label);
    void itemCompleted(Item *item);
    void itemUncompleted(Item *item);
    void deleteUndoItem(Item *item);
    void itemMoved(Item *item, qint64 projectId, qint64 oldProjectId, int index);
    void itemSectionMoved(Item *item, qint64 sectionId, qint64 oldSectionId);
    void itemParentMoved(Item *item, qint64 parentId, qint64 oldParentId);
    void itemSectionParentMoved(Item *item, qint64 sectionId, qint64 oldSectionId, qint64 parentId, qint64 oldParentId);
    void itemIdUpdated(qint64 currentId, qint64 newId);

    void labelAdded(Label *label);
    void labelDeleted(qint64 id);
    void labelUpdated(Label *label);

    void reminderAdded(Reminder *reminder);
    void reminderDeleted(qint64 id);

    void checkProjectCount(qint64 projectId);

    void opened();
    void reset();

    void showToastDelete(int count);
    void showUndoItem(Item *item, QString type = QString());
    
private:
    QList<Item *> m_itemsToDelete;
    QSqlDatabase m_db;
    QString m_dbPath;
};
