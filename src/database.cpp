// Copyright 2019 Alain M. (https://github.com/alainm23/planner)
// Copyright 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#include "database.h"
#include <QStandardPaths>
#include <QApplication>
#include <QDir>
#include <KLocalizedString>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QDebug>
#include <QDateTime>
#include "item.h"
#include "queue.h"
#include "label.h"
#include "collaborator.h"
#include "project.h"

const QString DRIVER(QStringLiteral("QSQLITE"));

Database::Database(QObject* parent)
    : QObject(parent)
{
    Q_ASSERT(QSqlDatabase::isDriverAvailable(DRIVER));
    Q_ASSERT(QDir().mkpath(QDir::cleanPath(QStandardPaths::writableLocation(QStandardPaths::DataLocation))));
    QSqlDatabase db = QSqlDatabase::addDatabase(DRIVER);
    const auto path = QDir::cleanPath(QStandardPaths::writableLocation(QStandardPaths::DataLocation) + QStringLiteral("/") + qApp->applicationName());
    db.setDatabaseName(path);
        if (!db.open()) {
        qCritical() << db.lastError() << "while opening database at" << path;
        qApp->exit();
    }
}

Database::~Database()
{
}

void Database::openDatabase()
{
    int rc = 0;
    m_dbPath = getDatabasePath();

    if (!createDatabase()) {
        qCritical() << "Error creating the database tables";
        qApp->exit();
    }

    auto query = QSqlQuery(QStringLiteral("PRAGMA foreign_keys = ON;"));
    query.exec();
    
    patchDatabase();
    // fire signal to tell that the database is ready
    Q_EMIT opened();
}

QString Database::getDatabasePath ()
{
    // TODO use configuration
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + QStringLiteral("/todomanager/");
}

void Database::resetAll()
{
}

void Database::patchDatabase()
{
}

void Database::addItemToDelete(Item *item)
{
    m_itemsToDelete.append(item);
    Q_EMIT showToastDelete(m_itemsToDelete.count());
}

void Database::removeItemToDelete()
{
    for (auto *item : qAsConst(m_itemsToDelete)) {
        // TODO deleteItem(item);
        if (item->isTodoist() == 1) {
            //Planner.todoist.add_delete_item (item);
        }
    }

    m_itemsToDelete.clear();
}

bool Database::createDatabase()
{
    auto statement = QStringLiteral(R"RAW(
        CREATE TABLE IF NOT EXISTS Projects (
            id               INTEGER PRIMARY KEY,
            area_id          INTEGER,
            name             TEXT NOT NULL,
            note             TEXT,
            due_date         TEXT,
            color            INTEGER,
            is_todoist       INTEGER,
            inbox_project    INTEGER,
            team_inbox       INTEGER,
            item_order       INTEGER,
            is_deleted       INTEGER,
            is_archived      INTEGER,
            is_favorite      INTEGER,
            is_sync          INTEGER,
            shared           INTEGER,
            is_kanban        INTEGER,
            show_completed   INTEGER,
            sort_order       INTEGER,
            parent_id        INTEGER,
            collapsed        INTEGER
        );
    )RAW");
    auto query = QSqlQuery(statement);
    if (!query.exec()) {
        return false;
    }

    qDebug() << "Table Projects created";

    statement = QStringLiteral(R"RAW(
        CREATE TABLE IF NOT EXISTS Sections (
            id              INTEGER PRIMARY KEY,
            name            TEXT,
            project_id      INTEGER,
            item_order      INTEGER,
            collapsed       INTEGER,
            sync_id         INTEGER,
            is_deleted      INTEGER,
            is_archived     INTEGER,
            date_archived   TEXT,
            date_added      TEXT,
            is_todoist      INTEGER,
            note            TEXT
        );
    )RAW");
    query = QSqlQuery(statement);
    if (!query.exec()) {
        return false;
    }

    qDebug() << "Table Sections created";

    statement = QStringLiteral(R"RAW(
        CREATE TABLE IF NOT EXISTS Items (
            id                  INTEGER PRIMARY KEY,
            project_id          INTEGER,
            section_id          INTEGER,
            user_id             INTEGER,
            assigned_by_uid     INTEGER,
            responsible_uid     INTEGER,
            sync_id             INTEGER,
            parent_id           INTEGER,
            priority            INTEGER,
            item_order          INTEGER,
            checked             INTEGER,
            is_deleted          INTEGER,
            content             TEXT NOT NULL,
            note                TEXT,
            due_date            TEXT,
            due_timezone        TEXT,
            due_string          TEXT,
            due_lang            TEXT,
            due_is_recurring    INTEGER,
            date_added          TEXT,
            date_completed      TEXT,
            date_updated        TEXT,
            is_todoist          INTEGER,
            day_order           INTEGER,
            collapsed           INTEGER
        );
    )RAW");
    query = QSqlQuery(statement);
    if (!query.exec()) {
        return false;
    }
    qDebug() << "Table Items created";

    statement = QStringLiteral(R"RAW(
        CREATE TABLE IF NOT EXISTS Reminders (
            id                  INTEGER PRIMARY KEY,
            notify_uid          INTEGER,
            item_id             INTEGER,
            service             TEXT,
            type                TEXT,
            due_date            TEXT,
            due_timezone        TEXT,
            due_is_recurring    INTEGER,
            due_string          TEXT,
            due_lang            TEXT,
            mm_offset           INTEGER,
            is_deleted          INTEGER,
            is_todoist          INTEGER,
            FOREIGN KEY (item_id) REFERENCES Items (id) ON DELETE CASCADE
        );
    )RAW");
    query = QSqlQuery(statement);
    if (!query.exec()) {
        return false;
    }

    qDebug() << "Table Remindes created";

    statement = QStringLiteral(R"RAW(
        CREATE TABLE IF NOT EXISTS Labels (
            id              INTEGER PRIMARY KEY,
            name            TEXT,
            color           INTEGER,
            item_order      INTEGER,
            is_deleted      INTEGER,
            is_favorite     INTEGER,
            is_todoist      INTEGER
        );
    )RAW");
    query = QSqlQuery(statement);
    if (!query.exec()) {
        return false;
    }

    qDebug() << "Table Labels created";

    statement = QStringLiteral(R"RAW(
        CREATE TABLE IF NOT EXISTS Items_Labels (
            id              INTEGER PRIMARY KEY,
            item_id         INTEGER,
            label_id        INTEGER,
            CONSTRAINT unique_track UNIQUE (item_id, label_id),
            FOREIGN KEY (item_id) REFERENCES Items (id) ON DELETE CASCADE,
            FOREIGN KEY (label_id) REFERENCES Labels (id) ON DELETE CASCADE
        );
    )RAW");
    query = QSqlQuery(statement);
    if (!query.exec()) {
        return false;
    }

    qDebug() << "Table Items Labels created";

    statement = QStringLiteral(R"RAW(
        CREATE TABLE IF NOT EXISTS Collaborators (
            id          INTEGER PRIMARY KEY,
            email       TEXT,
            full_name   TEXT,
            timezone    TEXT,
            image_id    TEXT
        );
    )RAW");
    query = QSqlQuery(statement);
    if (!query.exec()) {
        return false;
    }

    qDebug() << "Table Collaborators created";

    statement = QStringLiteral(R"RAW(
        CREATE TABLE IF NOT EXISTS Collaborator_States (
            id                  INTEGER PRIMARY KEY AUTOINCREMENT,
            collaborators_id    INTEGER
            state               TEXT,
            user_id             INTEGER,
            is_deleted          INTEGER,
            project_id          INTEGER,
            FOREIGN KEY (collaborators_id) REFERENCES Collaborators (id) ON DELETE CASCADE
        );
    )RAW");
    query = QSqlQuery(statement);
    if (!query.exec()) {
        return false;
    }

    qDebug() << "Table Collaborators_States created";

    statement = QStringLiteral(R"RAW(
        CREATE TABLE IF NOT EXISTS Queue (
            uuid       TEXT PRIMARY KEY,
            object_id  INTEGER,
            query      TEXT,
            temp_id    TEXT,
            args       TEXT,
            date_added TEXT
        );
    )RAW");
    query = QSqlQuery(statement);
    if (!query.exec()) {
        return false;
    }

    qDebug() << "Table Queue created";

    statement = QStringLiteral(R"RAW(
        CREATE TABLE IF NOT EXISTS CurTempIds (
            id          INTEGER PRIMARY KEY,
            temp_id     TEXT,
            object      TEXT
        );
    )RAW");
    query = QSqlQuery(statement);
    if (!query.exec()) {
        return false;
    }

    qDebug() << "Table CurTempIds created";

    statement = QStringLiteral(R"RAW(
        CREATE TABLE IF NOT EXISTS QuickFindRecents (
            id          INTEGER PRIMARY KEY AUTOINCREMENT,
            type        TEXT,
            object      TEXT,
            date_added  TEXT
        );
    )RAW");
    query = QSqlQuery(statement);
    if (!query.exec()) {
        return false;
    }
    qDebug() << "Table QuickFindRecents created";

    return true;
}

void Database::insertQuickFindRecents(QString type, QString object)
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        INSERT OR IGNORE INTO QuickFindRecents (type, object, date_added)
        VALUES (?, ?, ?);
    )RAW"));
    query.bindValue(0, type);
    query.bindValue(1, object);
    query.bindValue(2, QDateTime::currentDateTime().toString());
    
    query.exec();
}

QStringList Database::getQuickFindRecents()
{
    QSqlQuery query(QStringLiteral("SELECT * FROM QuickFindRecents ORDER BY date_added DESC LIMIT 10;"));
    
    QStringList var;
    query.exec();
    while (query.next()) {
        var.append(query.value(1).toString() + "___separator___" + query.value(2).toString());
    }
    return var;
}

bool Database::isDatabaseEmpty() const
{
    QSqlQuery query(QStringLiteral("SELECT COUNT (*) FROM Projects;"));
    query.exec();
    if (query.next()) {
        return query.value(0).toInt() <= 0;
    }
    return true;
}
    
bool Database::projectExist(qint64 project)
{
    QSqlQuery query;
    query.prepare(QStringLiteral("SELECT COUNT (*) FROM Projects WHERE id = ?"));
    query.bindValue(0, project);
    query.exec();
    if (query.next()) {
        return query.value(0).toInt() > 0;
    }
}

/* Queue */

bool Database::insertQueue(const Queue &queue) const
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        INSERT OR IGNORE INTO Queue (uuid, object_id, query, temp_id, args, date_added)
        VALUES (?, ?, ?, ?, ?, ?);
    )RAW"));
    
    query.bindValue(0, queue.uuid());
    query.bindValue(1, queue.objectId());
    query.bindValue(2, queue.query());
    query.bindValue(3, queue.temId());
    query.bindValue(4, queue.args());
    query.bindValue(5, queue.dateAdded().toString());
    
    if (query.exec()) {
        auto error = query.lastError();
        qWarning() << "Error " << error.type() << ": " << error.text();
        return false;
    }
    
    return true;
}

Queue &&Database::getQueueByObjectId(qint64 id) const
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        SELECT * FROM Queue WHERE object_id = ?;
    )RAW"));
    query.bindValue(0, id);
    query.exec();
    
    Queue queue;
    if (query.next()) {
        queue.setUuid(query.value(0).toString());
        queue.setObjectId(query.value(1).toInt());
        queue.setQuery(query.value(2).toString());
        queue.setTemId(query.value(3).toString());
        queue.setQuery(query.value(4).toString());
        queue.setDateAdded(QDateTime::fromString(query.value(5).toString()));
    }
    return std::move(queue);
}

QList<Queue *> &&Database::getAllQueue() const
{
    QSqlQuery query(QStringLiteral(R"RAW(
        SELECT * FROM Queue ORDER BY date_added;
    )RAW"));
    query.exec();
    
    QList<Queue *> queues;
    while (query.next()) {
        auto queue = new Queue;
        queue->setUuid(query.value(0).toString());
        queue->setObjectId(query.value(1).toInt());
        queue->setQuery(query.value(2).toString());
        queue->setTemId(query.value(3).toString());
        queue->setQuery(query.value(4).toString());
        queue->setDateAdded(QDateTime::fromString(query.value(5).toString()));
        queues.append(queue);
    }
    return std::move(queues);
}

void Database::updateQueue(const Queue& queue) const
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        UPDATE Queue SET object_id = ?, query = ?, temp_id = ?, args = ?
        WHERE uuid = ?;
    )RAW"));
    query.bindValue(0, queue.uuid());
    query.bindValue(1, queue.objectId());
    query.bindValue(2, queue.query());
    query.bindValue(3, queue.temId());
    query.bindValue(4, queue.args());
    query.bindValue(5, queue.dateAdded().toString());
    query.exec();
}

void Database::removeQueue(const QString &uuid) const
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        DELETE FROM Queue WHERE uuid = ?;
    )RAW"));
    query.bindValue(0, uuid);
    query.exec();
    
}

void Database::clearQueue() const
{
    QSqlQuery query(QStringLiteral(R"RAW(
        DELETE FROM Queue;
    )RAW"));
    if (!query.exec()) {
        qWarning() << "Error" << query.lastError();
    }
}

bool Database::insertCurTempIds(qint64 id, QString tempId, QString object)
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        INSERT OR IGNORE INTO CurTempIds (id, temp_id, object)
        VALUES (?, ?, ?);
    )RAW"));
    query.bindValue(0, id);
    query.bindValue(1, tempId);
    query.bindValue(2, object);
    if (!query.exec()) {
        qWarning() << query.lastError();
        return false;
    }
    return true;
}

void Database::removeCurTempIds(qint64 id)
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        DELETE FROM CurTempIds WHERE id = ?;
    )RAW"));
    query.addBindValue(id);
    if (!query.exec()) {
        qWarning() << query.lastError();
    }
}

void Database::clearCurTempIds()
{
    QSqlQuery query(QStringLiteral(R"RAW(
        DELETE FROM CurTempIds;
    )RAW"));
    
    if (!query.exec()) {
        qWarning() << query.lastError();
    }
}

bool Database::curTempIdsExists(qint64 id)
{
    bool returned = false;
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        SELECT COUNT (*) FROM CurTempIds WHERE id = ?;
    )RAW"));
    query.addBindValue(id);
    query.exec();
    
    if (query.next()) {
        returned = query.value(0).toInt() > 0;
    }
    return returned;
}

QString Database::getTempId(qint64 id)
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        SELECT temp_id FROM CurTempIds WHERE id = ?;
    )RAW"));
    
    query.addBindValue(id);
    
    query.exec();
    if (query.next()) {
        return query.value(0).toString();
    }
    return QString();
}


/*

    public bool column_exists (string table, string col) {
        Sqlite.Statement stmt;

        string sql = """
            SELECT * FROM %s LIMIT 1;
        """.printf (table);

        int res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        stmt.step ();

        for (int i = 0; i < stmt.column_count (); i++) {
            if (stmt.column_name (i) == col) {
                return true;
            }
        }

        return false;
    }

    public void add_int_column (string table, string col, int default_value) {
        Sqlite.Statement stmt;
        int res;
        string sql;

        sql = """
            ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %i;
        """.printf (table, col, default_value);

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
        }

        stmt.reset ();
    }

    public void add_int64_column (string table, string col, int64 default_value) {
        Sqlite.Statement stmt;
        int res;
        string sql;

        sql = """
            ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %s;
        """.printf (table, col, default_value.to_string ());

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
        }

        stmt.reset ();
    }

    public void add_text_column (string table, string col, string default_value) {
        Sqlite.Statement stmt;
        int res;
        string sql;

        sql = """
            ALTER TABLE %s ADD COLUMN %s TEXT DEFAULT '%s';
        """.printf (table, col, default_value);

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
        }

        stmt.reset ();
    }

    /*
        Collaborators
    */
    
bool Database::insertCollaborator(const Collaborator& collaborator)
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        INSERT OR IGNORE INTO Collaborators (id, email, full_name, timezone, image_id)
        VALUES (?, ?, ?, ?, ?);
    )RAW"));
    query.addBindValue(collaborator.id());
    query.addBindValue(collaborator.email());
    query.addBindValue(collaborator.fullName());
    query.addBindValue(collaborator.timezone().id());
    query.addBindValue(collaborator.imageId());
    if (!query.exec()) {
        qWarning() << query.lastError();
        return false;
    }
    return true;
}

Project *Database::createInboxProject() const
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        INSERT OR IGNORE INTO Projects (name, inbox_project, area_id)
        VALUES (?, ?, ?);
    )RAW"));
    
    auto project = new Project;
    project->setName(i18n("Inbox"));
    project->setInboxProject(true);
    query.addBindValue(project->name());
    query.addBindValue(project->inboxProject());
    query.addBindValue(project->areaId());
    query.exec();
    
    if (!query.exec()) {
        qWarning() << query.lastError();
    }
    return project;
}

int Database::getProjectCountByArea(qint64 id) const
{
    
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        SELECT COUNT (*) FROM Projects WHERE area_id = ?;
    )RAW"));
    query.addBindValue(id);
    query.exec();
    if (query.next()) {
        return query.value(0).toInt();
    }
    return 0;
}


bool Database::insertProject(Project* project)
{
    QSqlQuery query(QStringLiteral(R"RAW(
        SELECT COUNT (*) FROM Projects WHERE parent_id = 0;
    )RAW"));
    
    query.exec();
    if (query.next()) {
        project->setItemOrder(query.value(0).toInt());
    }
    
    QSqlQuery query2;
    query2.prepare(QStringLiteral(R"RAW(
        INSERT OR IGNORE INTO Projects (id, area_id, name, note, due_date, color,
            is_todoist, inbox_project, team_inbox, item_order, is_deleted, is_archived,
            is_favorite, is_sync, shared, show_completed, sort_order, parent_id,
            collapsed)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
    )RAW"));
    query2.addBindValue(project->id());
    query2.addBindValue(project->areaId());
    query2.addBindValue(project->name());
    query2.addBindValue(project->note());
    query2.addBindValue(project->dueDate().toString());
    query2.addBindValue(project->color());
    query2.addBindValue(project->isTodoist());
    query2.addBindValue(project->isTeamInbox());
    query2.addBindValue(project->itemOrder());
    query2.addBindValue(project->isDeleted());
    query2.addBindValue(project->isArchived());
    query2.addBindValue(project->isFavorite());
    query2.addBindValue(project->isSync());
    query2.addBindValue(project->shared());
    query2.addBindValue(project->showCompleted());
    query2.addBindValue(project->sortOrder());
    query2.addBindValue(project->collapsed());
    
    if (!query2.exec()) {
        qWarning() << query2.lastError();
        return false;
    }
    Q_EMIT projectAdded(project);
    return true;
}

void Database::updateProject(Project* project)
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        UPDATE Projects SET name = ?, note = ?, due_date = ?,
            color = ?, item_order = ?,
            is_deleted = ?, is_archived = ?, is_favorite = ?,
            is_sync = ?, shared = ?, is_kanban = ?, show_completed = ?,
            collapsed = ?
        WHERE id = ?;
    )RAW"));
    
    query.addBindValue(project->name());
    query.addBindValue(project->note());
    query.addBindValue(project->dueDate().toString());
    query.addBindValue(project->color());
    query.addBindValue(project->itemOrder());
    query.addBindValue(project->isDeleted());
    query.addBindValue(project->isArchived());
    query.addBindValue(project->isFavorite());
    query.addBindValue(project->isSync());
    query.addBindValue(project->shared());
    query.addBindValue(project->isKanban());
    query.addBindValue(project->showCompleted());
    query.addBindValue(project->id());
    
    if (query.exec()) {
        Q_EMIT projectUpdated(project);
    }
}


void Database::updateSortOrderProject(qint64 projectId, int orden)
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        UPDATE Projects SET sort_order = ? WHERE id = ?;
    )RAW"));
    
    query.addBindValue(orden);
    query.addBindValue(currentId);
    
    if (query.exec()) {
        // TODO Planner.event_bus.sort_items_project (project_id, orden);
    }
}

void Database::updateItemId(qint64 currentId, qint64 newId)
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        UPDATE Items SET id = ? WHERE id = ?;
    )RAW"));
    query.addBindValue(newId);
    query.addBindValue(currentId);
    if (query.exec()) {
        Q_EMIT itemIdUpdated(currentId, newId);
        QSqlQuery query;
        query.prepare(QStringLiteral(R"RAW(
            UPDATE Items SET parent_id = ? WHERE parent_id = ?;
        )RAW"));
        query.addBindValue(newId);
        query.addBindValue(currentId);
        query.exec();
    }
}

void Database::updateSectionId(qint64 currentId, qint64 newId)
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        UPDATE Sections SET id = ? WHERE id = ?;
    )RAW"));
    query.addBindValue(newId);
    query.addBindValue(currentId);
    if (query.exec()) {
        Q_EMIT sectionIdUpdated(currentId, newId);
        QSqlQuery query;
        query.prepare(QStringLiteral(R"RAW(
            UPDATE Items SET section_id = ? WHERE section_id = ?;
        )RAW"));
        query.addBindValue(newId);
        query.addBindValue(currentId);
        query.exec();
    }
}

void Database::updateProjectId(qint64 currentId, qint64 newId)
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        UPDATE Projects SET id = ? WHERE id = ?;
    )RAW"));
    query.addBindValue(newId);
    query.addBindValue(currentId);
    if (query.exec()) {
        Q_EMIT projectIdUpdated(currentId, newId);
        QSqlQuery query;
        query.prepare(QStringLiteral(R"RAW(
            UPDATE Sections SET project_id = ? WHERE project_id = ?;
        )RAW"));
        query.addBindValue(newId);
        query.addBindValue(currentId);
        if (query.exec()) {
            Q_EMIT projectIdUpdated(currentId, newId);
            QSqlQuery query;
            query.prepare(QStringLiteral(R"RAW(
                UPDATE Items SET project_id = ? WHERE project_id = ?;
            )RAW"));
            query.addBindValue(newId);
            query.addBindValue(currentId);
            query.exec();
        }
    }
}

void Database::deleteProject(qint64 id)
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        DELETE FROM Projects WHERE id = ?;
    )RAW"));
    query.addBindValue(id);
    
    if (!query.exec()) {
        qWarning() << query.lastError();
        return;
    }
    
    QSqlQuery query2;
    query2.prepare(QStringLiteral(R"RAW(
        DELETE FROM Sections WHERE project_id = ?;
    )RAW"));
    query2.addBindValue(id);
    
    if (!query2.exec()) {
        qWarning() << query2.lastError();
        return;
    }
    
    QSqlQuery query3;
    query3.prepare(QStringLiteral(R"RAW(
        DELETE FROM Items WHERE project_id = ?;
    )RAW"));
    query3.addBindValue(id);
    
    if (!query3.exec()) {
        qWarning() << query3.lastError();
        return;
    }
    Q_EMIT projectDeleted(id);
}

bool Database::moveProject(Project *project, qint64 parentId)
{
    const qint64 oldParentId = parentId;
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        SELECT COUNT (*) FROM Projects WHERE parent_id = ?;
    )RAW"));
    query.addBindValue(parentId);
    query.exec();
    if (query.next()) {
        project->setItemOrder(query.value(0).toInt());
    }
    
    QSqlQuery query2;
    query2.prepare(QStringLiteral(R"RAW(
        UPDATE Projects SET parent_id = ?, item_order = ? WHERE id = ?;
    )RAW"));
    query2.addBindValue(parentId);
    query2.addBindValue(project->itemOrder());
    query2.addBindValue(project->id());
    
    if (query2.exec()) {
        Q_EMIT projectMoved(project, parentId, oldParentId);
    }
}

QList<Project *> Database::getAllProjectsByParent(qint64 id) const
{
    QSqlQuery query;
    query.prepare(QStringLiteral(R"RAW(
        SELECT id, area_id, name, note, due_date, color, is_todoist, inbox_project, team_inbox,
            item_order, is_deleted, is_archived, is_favorite, is_sync, shared, is_kanban, show_completed,
            sort_order, parent_id, collapsed
        FROM Projects WHERE parent_id = ? ORDER BY item_order;
    )RAW"));
    query.addBindValue(id);
    
    QList<Project *> projects;
    while (query.next()) {
        auto p = new Project();
        p->setId(query.value(0).toLongLong());
        p->setAreaId(query.value(1).toLongLong());
        p->setName(query.value(2).toString());
        p->setNote(query.value(3).toString());
        p->setDueDate(QDateTime::fromString(query.value(4).toString()));
        p->setColor(query.value(5).toInt());
        p->setIsTodoist(query.value(6).toInt());
            p.color = stmt.column_int (5);
            p.is_todoist = stmt.column_int (6);
            p.inbox_project = stmt.column_int (7);
            p.team_inbox = stmt.column_int (8);
            p.item_order = stmt.column_int (9);
            p.is_deleted = stmt.column_int (10);
            p.is_archived = stmt.column_int (11);
            p.is_favorite = stmt.column_int (12);
            p.is_sync = stmt.column_int (13);
            p.shared = stmt.column_int (14);
            p.is_kanban = stmt.column_int (15);
            p.show_completed = stmt.column_int (16);
            p.sort_order = stmt.column_int (17);
            p.parent_id = stmt.column_int64 (18);
            p.collapsed = stmt.column_int (19);
        
}


    
    /*

    public Gee.ArrayList<Objects.Project?> get_all_projects_by_parent (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Project?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var p = new Objects.Project ();

            p.id = stmt.column_int64 (0);
            p.area_id = stmt.column_int64 (1);
            p.name = stmt.column_text (2);
            p.note = stmt.column_text (3);
            p.due_date = stmt.column_text (4);
            p.color = stmt.column_int (5);
            p.is_todoist = stmt.column_int (6);
            p.inbox_project = stmt.column_int (7);
            p.team_inbox = stmt.column_int (8);
            p.item_order = stmt.column_int (9);
            p.is_deleted = stmt.column_int (10);
            p.is_archived = stmt.column_int (11);
            p.is_favorite = stmt.column_int (12);
            p.is_sync = stmt.column_int (13);
            p.shared = stmt.column_int (14);
            p.is_kanban = stmt.column_int (15);
            p.show_completed = stmt.column_int (16);
            p.sort_order = stmt.column_int (17);
            p.parent_id = stmt.column_int64 (18);
            p.collapsed = stmt.column_int (19);

            all.add (p);
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Project?> get_all_projects_by_todoist () {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, area_id, name, note, due_date, color, is_todoist, inbox_project, team_inbox,
                item_order, is_deleted, is_archived, is_favorite, is_sync, shared, is_kanban, show_completed,
                sort_order, parent_id, collapsed
            FROM Projects WHERE is_todoist = 1;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Project?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var p = new Objects.Project ();

            p.id = stmt.column_int64 (0);
            p.area_id = stmt.column_int64 (1);
            p.name = stmt.column_text (2);
            p.note = stmt.column_text (3);
            p.due_date = stmt.column_text (4);
            p.color = stmt.column_int (5);
            p.is_todoist = stmt.column_int (6);
            p.inbox_project = stmt.column_int (7);
            p.team_inbox = stmt.column_int (8);
            p.item_order = stmt.column_int (9);
            p.is_deleted = stmt.column_int (10);
            p.is_archived = stmt.column_int (11);
            p.is_favorite = stmt.column_int (12);
            p.is_sync = stmt.column_int (13);
            p.shared = stmt.column_int (14);
            p.is_kanban = stmt.column_int (15);
            p.show_completed = stmt.column_int (16);
            p.sort_order = stmt.column_int (17);
            p.parent_id = stmt.column_int64 (18);
            p.collapsed = stmt.column_int (19);

            all.add (p);
        }

        stmt.reset ();
        return all;
    }

    public bool projects_area_exists (int64 id) {
        bool returned = false;
        Sqlite.Statement stmt;

        int res = db.prepare_v2 ("SELECT COUNT (*) FROM Projects WHERE area_id = ?", -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.ROW) {
            returned = stmt.column_int (0) > 0;
        }

        stmt.reset ();
        return returned;
    }

    public Gee.ArrayList<Objects.Project?> get_all_projects () {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, area_id, name, note, due_date, color, is_todoist, inbox_project, team_inbox,
                item_order, is_deleted, is_archived, is_favorite, is_sync, shared, is_kanban, show_completed,
                sort_order, parent_id, collapsed
            FROM Projects ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Project?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var p = new Objects.Project ();

            p.id = stmt.column_int64 (0);
            p.area_id = stmt.column_int64 (1);
            p.name = stmt.column_text (2);
            p.note = stmt.column_text (3);
            p.due_date = stmt.column_text (4);
            p.color = stmt.column_int (5);
            p.is_todoist = stmt.column_int (6);
            p.inbox_project = stmt.column_int (7);
            p.team_inbox = stmt.column_int (8);
            p.item_order = stmt.column_int (9);
            p.is_deleted = stmt.column_int (10);
            p.is_archived = stmt.column_int (11);
            p.is_favorite = stmt.column_int (12);
            p.is_sync = stmt.column_int (13);
            p.shared = stmt.column_int (14);
            p.is_kanban = stmt.column_int (15);
            p.show_completed = stmt.column_int (16);
            p.sort_order = stmt.column_int (17);
            p.parent_id = stmt.column_int64 (18);
            p.collapsed = stmt.column_int (19);

            all.add (p);
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Project?> get_all_projects_by_search (string search_text) {
        Sqlite.Statement stmt;
        string sql;
        int res;
        
        sql = """
            SELECT id, area_id, name, note, due_date, color, is_todoist, inbox_project, team_inbox,
                item_order, is_deleted, is_archived, is_favorite, is_sync, shared, is_kanban, show_completed,
                sort_order, parent_id, collapsed
            FROM Projects WHERE name LIKE ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (1, "%" + search_text + "%");
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Project?> ();
        while ((res = stmt.step ()) == Sqlite.ROW) {
            var p = new Objects.Project ();

            p.id = stmt.column_int64 (0);
            p.area_id = stmt.column_int64 (1);
            p.name = stmt.column_text (2);
            p.note = stmt.column_text (3);
            p.due_date = stmt.column_text (4);
            p.color = stmt.column_int (5);
            p.is_todoist = stmt.column_int (6);
            p.inbox_project = stmt.column_int (7);
            p.team_inbox = stmt.column_int (8);
            p.item_order = stmt.column_int (9);
            p.is_deleted = stmt.column_int (10);
            p.is_archived = stmt.column_int (11);
            p.is_favorite = stmt.column_int (12);
            p.is_sync = stmt.column_int (13);
            p.shared = stmt.column_int (14);
            p.is_kanban = stmt.column_int (15);
            p.show_completed = stmt.column_int (16);
            p.sort_order = stmt.column_int (17);
            p.parent_id = stmt.column_int64 (18);
            p.collapsed = stmt.column_int (19);

            all.add (p);
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Section?> get_all_sections_by_search (string search_text) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, name, project_id, item_order, collapsed, sync_id, is_deleted, is_archived,
                date_archived, date_added, is_todoist, note
            FROM Sections WHERE name LIKE ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (1, "%" + search_text + "%");
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Section?> ();
        while ((res = stmt.step ()) == Sqlite.ROW) {
            var s = new Objects.Section ();

            s.id = stmt.column_int64 (0);
            s.name = stmt.column_text (1);
            s.project_id = stmt.column_int64 (2);
            s.item_order = stmt.column_int (3);
            s.collapsed = stmt.column_int (4);
            s.sync_id = stmt.column_int64 (5);
            s.is_deleted = stmt.column_int (6);
            s.is_archived = stmt.column_int (7);
            s.date_archived = stmt.column_text (8);
            s.date_added = stmt.column_text (9);
            s.is_todoist = stmt.column_int (10);
            s.note = stmt.column_text (11);

            all.add (s);
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Project?> get_all_projects_no_parent () {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, area_id, name, note, due_date, color, is_todoist, inbox_project, team_inbox,
                item_order, is_deleted, is_archived, is_favorite, is_sync, shared, is_kanban, show_completed,
                sort_order, parent_id, collapsed
            FROM Projects WHERE inbox_project = 0 AND parent_id = 0 ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Project?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var p = new Objects.Project ();

            p.id = stmt.column_int64 (0);
            p.area_id = stmt.column_int64 (1);
            p.name = stmt.column_text (2);
            p.note = stmt.column_text (3);
            p.due_date = stmt.column_text (4);
            p.color = stmt.column_int (5);
            p.is_todoist = stmt.column_int (6);
            p.inbox_project = stmt.column_int (7);
            p.team_inbox = stmt.column_int (8);
            p.item_order = stmt.column_int (9);
            p.is_deleted = stmt.column_int (10);
            p.is_archived = stmt.column_int (11);
            p.is_favorite = stmt.column_int (12);
            p.is_sync = stmt.column_int (13);
            p.shared = stmt.column_int (14);
            p.is_kanban = stmt.column_int (15);
            p.show_completed = stmt.column_int (16);
            p.sort_order = stmt.column_int (17);
            p.parent_id = stmt.column_int64 (18);
            p.collapsed = stmt.column_int (19);

            all.add (p);
        }

        stmt.reset ();
        return all;
    }

    public Objects.Project? get_project_by_id (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, area_id, name, note, due_date, color, is_todoist, inbox_project, team_inbox,
                item_order, is_deleted, is_archived, is_favorite, is_sync, shared, is_kanban, show_completed,
                sort_order, parent_id, collapsed
            FROM Projects WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var p = new Objects.Project ();

        if (stmt.step () == Sqlite.ROW) {
            p.id = stmt.column_int64 (0);
            p.area_id = stmt.column_int64 (1);
            p.name = stmt.column_text (2);
            p.note = stmt.column_text (3);
            p.due_date = stmt.column_text (4);
            p.color = stmt.column_int (5);
            p.is_todoist = stmt.column_int (6);
            p.inbox_project = stmt.column_int (7);
            p.team_inbox = stmt.column_int (8);
            p.item_order = stmt.column_int (9);
            p.is_deleted = stmt.column_int (10);
            p.is_archived = stmt.column_int (11);
            p.is_favorite = stmt.column_int (12);
            p.is_sync = stmt.column_int (13);
            p.shared = stmt.column_int (14);
            p.is_kanban = stmt.column_int (15);
            p.show_completed = stmt.column_int (16);
            p.sort_order = stmt.column_int (17);
            p.parent_id = stmt.column_int64 (18);
            p.collapsed = stmt.column_int (19);
        }

        stmt.reset ();
        return p;
    }

    public void update_project_item_order (int64 project_id, int64 parent_id, int item_order) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            UPDATE Projects SET parent_id = ?, item_order = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, parent_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (2, item_order);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (3, project_id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {
            
        }

        stmt.reset ();
    }
    
    public void update_label_item_order (int64 id, int item_order) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            UPDATE Labels SET item_order = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (1, item_order);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {
            //updated_playlist (playlist);
        }

        stmt.reset ();
    }

    public int get_project_count (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;
        int items_0 = 0;
        int items_1 = 0;

        sql = """
            SELECT checked, count(checked) FROM Items WHERE project_id = ? GROUP BY checked ORDER BY count (checked);
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        while ((res = stmt.step ()) == Sqlite.ROW) {
            if (stmt.column_int (0) == 0) {
                items_0 = stmt.column_int (1);
            } else {
                items_1 = stmt.column_int (1);
            }
        }

        update_project_count (id, items_0, items_1);
        stmt.reset ();
        return items_0;
    }

    public int get_today_project_count (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;
        int returned = 0;

        sql = """
            SELECT id, due_date FROM Items WHERE project_id = ? AND due_date != '';
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var due = new GLib.DateTime.from_iso8601 (stmt.column_text (1), new GLib.TimeZone.local ());
            if (Planner.utils.is_today (due)) {
                returned++;
            }
        }

        stmt.reset ();
        return returned;
    }

    public int get_today_count () {
        Sqlite.Statement stmt;
        string sql;
        int res;
        int count = 0;

        sql = """
            SELECT due_date FROM Items WHERE checked = 0 AND due_date != '';
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var due = new GLib.DateTime.from_iso8601 (stmt.column_text (0), new GLib.TimeZone.local ());
            if (Planner.utils.is_today (due)) {
                count++;
            }
        } 

        stmt.reset ();
        return count;
    }

    public int get_past_count () {
        Sqlite.Statement stmt;
        string sql;
        int res;
        int count = 0;

        sql = """
            SELECT due_date FROM Items WHERE checked = 0 AND due_date != '';
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var due = new GLib.DateTime.from_iso8601 (stmt.column_text (0), new GLib.TimeZone.local ());
            if (Planner.utils.is_overdue (due)) {
                count++;
            }
        }

        stmt.reset ();
        return count;
    }

    /*
        Labels
    */

    /*
    public bool label_name_exists (string name) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT count(*) FROM Labels WHERE name = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (1, name);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.ROW && stmt.column_int (0) <= 0) {
            return false;
        }

        return true;
    }

    public bool insert_label (Objects.Label label) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT count(*) FROM Labels WHERE name = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (1, label.name);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.ROW && stmt.column_int (0) <= 0) {
            stmt.reset ();

            sql = """
                INSERT OR IGNORE INTO Labels (id, name, color, item_order, is_deleted, is_favorite, is_todoist)
                VALUES (?, ?, ?, ?, ?, ?, ?);
            """;

            res = db.prepare_v2 (sql, -1, out stmt);
            assert (res == Sqlite.OK);

            res = stmt.bind_int64 (1, label.id);
            assert (res == Sqlite.OK);

            res = stmt.bind_text (2, label.name);
            assert (res == Sqlite.OK);

            res = stmt.bind_int (3, label.color);
            assert (res == Sqlite.OK);

            res = stmt.bind_int (4, label.item_order);
            assert (res == Sqlite.OK);

            res = stmt.bind_int (5, label.is_deleted);
            assert (res == Sqlite.OK);

            res = stmt.bind_int (6, label.is_favorite);
            assert (res == Sqlite.OK);

            res = stmt.bind_int (7, label.is_todoist);
            assert (res == Sqlite.OK);

            if (stmt.step () == Sqlite.DONE) {
                label_added (label);
                stmt.reset ();
                return true;
            } else {
                warning ("Error: %d: %s", db.errcode (), db.errmsg ());
                stmt.reset ();
                return false;
            }   
        } else {
            return false;
        }
    }

    public Gee.ArrayList<Objects.Label?> get_labels_by_search (string search_text) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, name, color, item_order, is_deleted, is_favorite, is_todoist
            FROM Labels WHERE name LIKE ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (1, "%" + search_text + "%");
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Label?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var l = new Objects.Label ();

            l.id = stmt.column_int64 (0);
            l.name = stmt.column_text (1);
            l.color = stmt.column_int (2);
            l.item_order = stmt.column_int (3);
            l.is_deleted = stmt.column_int (4);
            l.is_favorite = stmt.column_int (5);
            l.is_todoist = stmt.column_int (6);

            all.add (l);
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Label?> get_all_labels () {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, name, color, item_order, is_deleted, is_favorite, is_todoist
            FROM Labels ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Label?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var l = new Objects.Label ();

            l.id = stmt.column_int64 (0);
            l.name = stmt.column_text (1);
            l.color = stmt.column_int (2);
            l.item_order = stmt.column_int (3);
            l.is_deleted = stmt.column_int (4);
            l.is_favorite = stmt.column_int (5);
            l.is_todoist = stmt.column_int (6);

            all.add (l);
        }

        stmt.reset ();
        return all;
    }

    public Objects.Label get_label_by_id (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, name, color, item_order, is_deleted, is_favorite, is_todoist
            FROM Labels WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var l = new Objects.Label ();

        if (stmt.step () == Sqlite.ROW) {
            l.id = stmt.column_int64 (0);
            l.name = stmt.column_text (1);
            l.color = stmt.column_int (2);
            l.item_order = stmt.column_int (3);
            l.is_deleted = stmt.column_int (4);
            l.is_favorite = stmt.column_int (5);
            l.is_todoist = stmt.column_int (6);
        }

        stmt.reset ();
        return l;
    }

    public bool delete_label (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            DELETE FROM Labels WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {
            label_deleted (id);
            stmt.reset ();
            return true;
        } else {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
            stmt.reset ();
            return false;
        }
    }

    public bool update_label (Objects.Label label) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            UPDATE Labels SET name = ?, color = ?, item_order = ?, is_deleted = ?, 
            is_favorite = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (1, label.name);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (2, label.color);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (3, label.item_order);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (4, label.is_deleted);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (5, label.is_favorite);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (6, label.id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {
            label_updated (label);
            stmt.reset ();
            return true;
        } else {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
            stmt.reset ();
            return false;
        }
    }

    public void delete_label_todoist () {
        foreach (var label in get_all_labels ()) {
            if (label.is_todoist == 1) {
                delete_label (label.id);
            }
        }
    }
    
    /*
        Sections
    */
    /*

    public bool section_exists (int64 id) {
        bool returned = false;
        Sqlite.Statement stmt;

        int res = db.prepare_v2 ("SELECT COUNT (*) FROM Sections WHERE id = ?", -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.ROW) {
            returned = stmt.column_int (0) > 0;
        }

        stmt.reset ();
        return returned;
    }

    public bool label_exists (int64 id) {
        bool returned = false;
        Sqlite.Statement stmt;

        int res = db.prepare_v2 ("SELECT COUNT (*) FROM Labels WHERE id = ?", -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.ROW) {
            returned = stmt.column_int (0) > 0;
        }

        stmt.reset ();
        return returned;
    }

    public bool insert_section (Objects.Section section, int index=-1) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        section.item_order = 0;

        sql = """
            INSERT OR IGNORE INTO Sections (id, name, project_id, item_order, collapsed,
            sync_id, is_deleted, is_archived, date_archived, date_added, is_todoist, note)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, section.id);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (2, section.name);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (3, section.project_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (4, section.item_order);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (5, section.collapsed);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (6, section.sync_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (7, section.is_deleted);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (8, section.is_archived);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (9, section.date_archived);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (10, section.date_added);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (11, section.is_todoist);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (12, section.note);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
            stmt.reset ();
            return false;
        } else {
            section_added (section, index);
            stmt.reset ();
            return true;
        }
    }

    public Objects.Section get_section_by_id (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, name, project_id, item_order, collapsed, sync_id,
                is_deleted, is_archived, date_archived, date_added, is_todoist, note
            FROM Sections WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var s = new Objects.Section ();

        if (stmt.step () == Sqlite.ROW) {
            s.id = stmt.column_int64 (0);
            s.name = stmt.column_text (1);
            s.project_id = stmt.column_int64 (2);
            s.item_order = stmt.column_int (3);
            s.collapsed = stmt.column_int (4);
            s.sync_id = stmt.column_int64 (5);
            s.is_deleted = stmt.column_int (6);
            s.is_archived = stmt.column_int (7);
            s.date_archived = stmt.column_text (8);
            s.date_added = stmt.column_text (9);
            s.is_todoist = stmt.column_int (10);
            s.note = stmt.column_text (11);
        }

        stmt.reset ();
        return s;
    }

    public Gee.ArrayList<Objects.Section?> get_all_sections () {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, name, project_id, item_order, collapsed, sync_id, is_deleted, is_archived,
                date_archived, date_added, is_todoist, note
            FROM Sections ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Section?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var s = new Objects.Section ();

            s.id = stmt.column_int64 (0);
            s.name = stmt.column_text (1);
            s.project_id = stmt.column_int64 (2);
            s.item_order = stmt.column_int (3);
            s.collapsed = stmt.column_int (4);
            s.sync_id = stmt.column_int64 (5);
            s.is_deleted = stmt.column_int (6);
            s.is_archived = stmt.column_int (7);
            s.date_archived = stmt.column_text (8);
            s.date_added = stmt.column_text (9);
            s.is_todoist = stmt.column_int (10);
            s.note = stmt.column_text (11);

            all.add (s);
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Section?> get_all_sections_by_project (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, name, project_id, item_order, collapsed, sync_id, is_deleted, is_archived,
                date_archived, date_added, is_todoist, note
            FROM Sections WHERE project_id = ? ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Section?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var s = new Objects.Section ();

            s.id = stmt.column_int64 (0);
            s.name = stmt.column_text (1);
            s.project_id = stmt.column_int64 (2);
            s.item_order = stmt.column_int (3);
            s.collapsed = stmt.column_int (4);
            s.sync_id = stmt.column_int64 (5);
            s.is_deleted = stmt.column_int (6);
            s.is_archived = stmt.column_int (7);
            s.date_archived = stmt.column_text (8);
            s.date_added = stmt.column_text (9);
            s.is_todoist = stmt.column_int (10);
            s.note = stmt.column_text (11);

            all.add (s);
        }

        stmt.reset ();
        return all;
    }

    public void update_section (Objects.Section section) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            UPDATE Sections SET name = ?, collapsed = ?, item_order = ?, note = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (1, section.name);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (2, section.collapsed);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (3, section.item_order);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (4, section.note);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (5, section.id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {
            section_updated (section);
        } else {
            print ("Error: %d: %s\n".printf (db.errcode (), db.errmsg ()));
        }

        stmt.reset ();
    }

    public void delete_section (Objects.Section section) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            DELETE FROM Sections WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, section.id);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
        } else {
            stmt.reset ();

            sql = """
                DELETE FROM Items WHERE section_id = ?;
            """;

            res = db.prepare_v2 (sql, -1, out stmt);
            assert (res == Sqlite.OK);

            res = stmt.bind_int64 (1, section.id);
            assert (res == Sqlite.OK);

            if (stmt.step () != Sqlite.DONE) {
                warning ("Error: %d: %s", db.errcode (), db.errmsg ());
            } else {
                section_deleted (section);
            }
        }

        stmt.reset ();
    }

    public bool move_section (Objects.Section section, int64 project_id) {
        Sqlite.Statement stmt;
        string sql;
        int res;
        int64 old_project_id = section.project_id;

        sql = """
            UPDATE Sections SET project_id = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, project_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, section.id);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
            stmt.reset ();
            return false;
        } else {
            section_moved (section, project_id, old_project_id);

            stmt.reset ();

            sql = """
                UPDATE Items SET project_id = ? WHERE section_id = ?;
            """;

            res = db.prepare_v2 (sql, -1, out stmt);
            assert (res == Sqlite.OK);

            res = stmt.bind_int64 (1, project_id);
            assert (res == Sqlite.OK);

            res = stmt.bind_int64 (2, section.id);
            assert (res == Sqlite.OK);

            stmt.step ();
            stmt.reset ();
            return true;
        }
    }

    public void update_section_item_order (int64 section_id, int item_order) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            UPDATE Sections SET item_order = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (1, item_order);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, section_id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {
            //updated_playlist (playlist);
        }

        stmt.reset ();
    }

    /*
        Items
    */
/*
    public bool item_exists (int64 id) {
        bool returned = false;
        Sqlite.Statement stmt;

        int res = db.prepare_v2 ("SELECT COUNT (*) FROM Items WHERE id = ?", -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.ROW) {
            returned = stmt.column_int (0) > 0;
        }

        stmt.reset ();
        return returned;
    }

    public bool insert_item (Objects.Item item, int index=-1) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        if (index == -1) {
            sql = """
                SELECT COUNT (*) FROM Items WHERE project_id = ? AND section_id = ?;
            """;

            res = db.prepare_v2 (sql, -1, out stmt);
            assert (res == Sqlite.OK);

            res = stmt.bind_int64 (1, item.project_id);
            assert (res == Sqlite.OK);

            res = stmt.bind_int64 (2, item.section_id);
            assert (res == Sqlite.OK);

            if (stmt.step () == Sqlite.ROW) {
                item.item_order = stmt.column_int (0);
            }

            stmt.reset ();
        } else {
            item.item_order = index;
        }

        sql = """
            INSERT OR IGNORE INTO Items (id, project_id, section_id, user_id, assigned_by_uid,
            responsible_uid, sync_id, parent_id, priority, item_order, checked,
            is_deleted, content, note, due_date, date_added, date_completed, date_updated,
            due_timezone, due_string, due_lang, due_is_recurring, is_todoist)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, item.id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, item.project_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (3, item.section_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (4, item.user_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (5, item.assigned_by_uid);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (6, item.responsible_uid);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (7, item.sync_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (8, item.parent_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (9, item.priority);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (10, item.item_order);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (11, item.checked);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (12, item.is_deleted);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (13, item.content);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (14, item.note);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (15, item.due_date);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (16, item.date_added);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (17, item.date_completed);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (18, item.date_updated);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (19, item.due_timezone);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (20, item.due_string);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (21, item.due_lang);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (22, item.due_is_recurring);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (23, item.is_todoist);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
            stmt.reset ();
            return false;
        } else {
            item_added (item, index);
            stmt.reset ();
            return true;
        }
    }

    public void update_item_project_id (Objects.Item item, int64 project_id) {
        Sqlite.Statement stmt;
        string sql;
        int res;
        int64 old_project_id = item.project_id;
        item.project_id = project_id;

        sql = """
            UPDATE Items SET project_id = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, item.project_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
        } else {
            check_project_count (old_project_id);
            check_project_count (item.project_id);
        }

        stmt.reset ();
    }

    public bool update_item (Objects.Item item) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            UPDATE Items SET content = ?, note = ?, due_date = ?, is_deleted = ?, checked = ?, 
            item_order = ?, project_id = ?, section_id = ?, date_completed = ?, date_updated = ?,
            priority = ?, collapsed = ?
            WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (1, item.content);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (2, item.note);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (3, item.due_date);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (4, item.is_deleted);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (5, item.checked);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (6, item.item_order);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (7, item.project_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (8, item.section_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (9, item.date_completed);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (10, item.date_updated);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (11, item.priority);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (12, item.collapsed);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (13, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {
            item_updated (item);
            stmt.reset ();
            return true;
        } else {
            stmt.reset ();
            return false;
        }
    }

    public Objects.Item? get_item_by_id (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var i = new Objects.Item ();

        if (stmt.step () == Sqlite.ROW) {
            i = create_item_from_stmt (stmt);
        }

        stmt.reset ();
        return i;
    }

    public void update_item_completed (Objects.Item item, bool undo=false) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            UPDATE Items SET checked = ?, date_completed = ? WHERE id = ? OR parent_id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (1, item.checked);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (2, item.date_completed);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (3, item.id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (4, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {
            if (item.checked == 1 && undo == false) {
                item_completed (item);
            } else {
                item_uncompleted (item);
            }
        }

        stmt.reset ();
    }

    public void update_item_recurring_due_date (Objects.Item item, int value=1) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        GLib.DateTime next_due = Planner.utils.get_next_recurring_due_date (item, value);
        item.due_date = next_due.to_string ();

        sql = """
            UPDATE Items SET due_date = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (1, item.due_date);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
        } else {
            update_due_item (item, -1);
            foreach (var check in Planner.database.get_all_cheks_by_item (item.id)) {
                if (check.checked == 1) {
                    check.checked = 0;
                    check.date_completed = "";

                    Planner.database.update_item_completed (check, false);
                    if (item.is_todoist == 1) {
                        Planner.todoist.item_complete (check);
                    }
                }
            }
            
            if (item.is_todoist == 1) {
                Planner.todoist.update_item (item);
            }
        }

        stmt.reset ();
    }

    public void delete_item (Objects.Item item) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            DELETE FROM Items WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
        } else {
            stmt.reset ();

            sql = """
                DELETE FROM Items WHERE parent_id = ?;
            """;

            res = db.prepare_v2 (sql, -1, out stmt);
            assert (res == Sqlite.OK);

            res = stmt.bind_int64 (1, item.id);
            assert (res == Sqlite.OK);

            if (stmt.step () != Sqlite.DONE) {
                warning ("Error: %d: %s", db.errcode (), db.errmsg ());
            } else {
                item_deleted (item);
            }
        }

        stmt.reset ();
    }

    public bool move_item (Objects.Item item, int64 project_id, int section_id=0, int index=-1) {
        Sqlite.Statement stmt;
        string sql;
        int res;
        int64 old_project_id = item.project_id;
        item.section_id = section_id;

        subtract_task_counter (old_project_id);

        sql = """
            UPDATE Items SET project_id = ?, section_id = ?, parent_id = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, project_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, section_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (3, item.parent_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (4, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
            stmt.reset ();
            return false;
        } else {
            item_moved (item, project_id, old_project_id, index);

            stmt.reset ();

            sql = """
                UPDATE Items SET project_id = ? WHERE parent_id = ?;
            """;

            res = db.prepare_v2 (sql, -1, out stmt);
            assert (res == Sqlite.OK);

            res = stmt.bind_int64 (1, project_id);
            assert (res == Sqlite.OK);

            res = stmt.bind_int64 (2, item.id);
            assert (res == Sqlite.OK);

            stmt.step ();
            stmt.reset ();
            return true;
        }
    }

    public void update_item_order (Objects.Item item, int64 section_id, int item_order) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            UPDATE Items SET item_order = ?, section_id = ?, parent_id = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (1, item_order);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, section_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (3, 0);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (4, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {
            //updated_playlist (playlist);
        }

        stmt.reset ();
    }

    public void update_today_day_order (Objects.Item item, int day_order) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            UPDATE Items SET day_order = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (1, day_order);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {

        }

        stmt.reset ();
    }

    public void update_check_order (Objects.Item item, int64 parent_id, int item_order) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            UPDATE Items SET item_order = ?, parent_id = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (1, item_order);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, parent_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (3, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {
            //updated_playlist (playlist);
        }

        stmt.reset ();
    }

    public bool set_due_item (Objects.Item item, bool new_date, int index=-1) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        if (item.due_date == "") {
            item.due_string = "";
            item.due_lang = "";
            item.due_is_recurring = 0;
        }

        sql = """
            UPDATE Items SET due_date = ?, due_string = ?, due_lang = ?, due_is_recurring = ?
            WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (1, item.due_date);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (2, item.due_string);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (3, item.due_lang);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (4, item.due_is_recurring);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (5, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {
            if (new_date) {
                add_due_item (item, index);
            } else {
                if (item.due_date == "") {
                    remove_due_item (item);
                } else {
                    update_due_item (item, index);
                }
            }

            stmt.reset ();
            return true;
        }

        stmt.reset ();
        return false;
    }

    public int get_all_count_items_by_project (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id FROM Items WHERE project_id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var size = 0;
        while ((res = stmt.step ()) == Sqlite.ROW) {
            size++;
        }

        stmt.reset ();
        return size;
    }

    public int get_all_count_items_by_parent (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id FROM Items WHERE parent_id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var size = 0;
        while ((res = stmt.step ()) == Sqlite.ROW) {
            size++;
        }

        stmt.reset ();
        return size;
    }

    public int get_count_checked_items_by_parent (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id FROM Items WHERE parent_id = ? AND checked = 1;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var size = 0;
        while ((res = stmt.step ()) == Sqlite.ROW) {
            size++;
        }

        stmt.reset ();
        return size;
    }

    public int get_count_items_by_project (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id FROM Items WHERE project_id = ? AND checked = 0;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var size = 0;
        while ((res = stmt.step ()) == Sqlite.ROW) {
            size++;
        }

        stmt.reset ();
        return size;
    }

    public int get_count_checked_items_by_project (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id FROM Items WHERE project_id = ? AND checked = 1;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var size = 0;
        while ((res = stmt.step ()) == Sqlite.ROW) {
            size++;
        }

        stmt.reset ();
        return size;
    }

    public int get_count_sections_by_project (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id FROM Sections WHERE project_id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var size = 0;
        while ((res = stmt.step ()) == Sqlite.ROW) {
            size++;
        }

        stmt.reset ();
        return size;
    }

    public Gee.ArrayList<Objects.Item?> get_all_completed_items_by_inbox (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE project_id = ? AND checked = 1 ORDER BY date_completed;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_all_completed_items_by_project (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE project_id = ? AND checked = 1 AND parent_id = 0 ORDER BY date_completed;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_all_items () {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Item?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var i = create_item_from_stmt (stmt);

            all.add (i);
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Item?> get_all_items_uncompleted () {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE checked = 0;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Item?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var i = create_item_from_stmt (stmt);

            all.add (i);
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Item?> get_all_items_by_project (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE project_id = ? ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_all_completed_items () {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE checked = 1 ORDER BY date_completed DESC;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_all_items_by_inbox (int64 id, int is_todoist) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE project_id = ? AND section_id = 0 AND parent_id = 0 AND checked = 0 ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_all_items_by_project_no_section_no_parent (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE project_id = ? AND section_id = 0 AND parent_id = 0 AND checked = 0 ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_all_completed_items_by_project_no_section_no_parent (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE project_id = ? AND section_id = 0 AND parent_id = 0 AND checked = 1 ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_all_items_by_project_no_section (Objects.Project project) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE project_id = ? AND section_id = 0 ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, project.id);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_all_items_by_section_no_parent (Objects.Section section) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE section_id = ? AND parent_id = 0 AND checked = 0 ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, section.id);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_all_completed_items_by_section_no_parent (Objects.Section section) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE section_id = ? AND parent_id = 0 AND checked = 1 ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, section.id);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_all_items_by_section (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE section_id = ? ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_all_cheks_by_item (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE parent_id = ? ORDER BY item_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_all_today_items () {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE checked = 0 AND due_date != '' ORDER BY day_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        var items = get_items_from_stmt (stmt);
        var all = new Gee.ArrayList<Objects.Item?> ();

        foreach (var i in items) {
            var due = new GLib.DateTime.from_iso8601 (i.due_date, new GLib.TimeZone.local ());
            if (Planner.utils.is_today (due)) {
                  all.add (i);
            }
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Item?> get_all_today_completed_items () {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE checked = 1 AND due_date != '' ORDER BY day_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Item?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var i = create_item_from_stmt (stmt);

            var due = new GLib.DateTime.from_iso8601 (i.due_date, new GLib.TimeZone.local ());
            if (Planner.utils.is_today (due)) {
                  all.add (i);
            }
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Item?> get_all_overdue_items () {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE checked = 0 AND due_date != '' ORDER BY day_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        var items = get_items_from_stmt (stmt);
        var all = new Gee.ArrayList<Objects.Item?> ();

        foreach (var i in items) {
            var due = new GLib.DateTime.from_iso8601 (i.due_date, new GLib.TimeZone.local ());
            if (Planner.utils.is_overdue (due)) {
                  all.add (i);
            }
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Item?> get_items_by_date (GLib.DateTime date) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE checked = 0 ORDER BY day_order;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        var items = get_items_from_stmt (stmt);
        var all = new Gee.ArrayList<Objects.Item?> ();

        foreach (var i in items) {
            if (i.due_date != "") {
                var due = new GLib.DateTime.from_iso8601 (i.due_date, new GLib.TimeZone.local ());
                if (Granite.DateTime.is_same_day (due, date)) {
                    all.add (i);
                }
            }
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Item?> get_items_by_priority (int priority) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE priority = ? AND checked = 0;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int (1, priority);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_items_by_search (string search_text) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, project_id, section_id, user_id, assigned_by_uid, responsible_uid,
                sync_id, parent_id, priority, item_order, checked, is_deleted, content, note,
                due_date, due_timezone, due_string, due_lang, due_is_recurring, date_added,
                date_completed, date_updated, is_todoist, day_order, collapsed
            FROM Items WHERE checked = 0 AND (content LIKE ? OR note LIKE ?);
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (1, "%" + search_text + "%");
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public Gee.ArrayList<Objects.Item?> get_items_by_label (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT Items.id, Items.project_id, Items.section_id, Items.user_id, Items.assigned_by_uid, Items.responsible_uid,
                Items.sync_id, Items.parent_id, Items.priority, Items.item_order, Items.checked, Items.is_deleted, Items.content, Items.note,
                Items.due_date, Items.due_timezone, Items.due_string, Items.due_lang, Items.due_is_recurring, Items.date_added,
                Items.date_completed, Items.date_updated, Items.is_todoist, Items.day_order, Items.collapsed
            FROM Items_Labels
            INNER JOIN Items ON Items.id = Items_Labels.item_id WHERE Items_Labels.label_id = ? AND Items.checked = 0;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        return get_items_from_stmt (stmt);
    }

    public bool add_item_label (int64 item_id, Objects.Label label) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        int64 id = Planner.utils.generate_id ();

        sql = """
            INSERT OR IGNORE INTO Items_Labels (id, item_id, label_id)
            VALUES (?, ?, ?);
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, item_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (3, label.id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.DONE) {
            item_label_added (id, item_id, label);
            stmt.reset ();
            return true;
        } else {
            stmt.reset ();
            return false;
        }
    }

    public Gee.ArrayList<Objects.Label?> get_labels_by_item (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT Items_Labels.id, Items_Labels.label_id, Labels.name, Labels.color, Labels.is_todoist
                FROM Items_Labels
                INNER JOIN Labels ON Items_Labels.label_id = Labels.id
            WHERE item_id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Label?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var l = new Objects.Label ();

            l.item_label_id = stmt.column_int64 (0);
            l.id = stmt.column_int64 (1);
            l.name = stmt.column_text (2);
            l.color = stmt.column_int (3);
            l.is_todoist = stmt.column_int (4);

            all.add (l);
        }

        stmt.reset ();
        return all;
    }

    public Gee.ArrayList<Objects.Label?> get_labels_by_project (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT Items_Labels.id, Items_Labels.label_id, Labels.name, Labels.color, Items.project_id FROM Items_Labels
            INNER JOIN Labels ON Items_Labels.label_id = Labels.id
            INNER JOIN Items ON Items_Labels.item_id = Items.id
            WHERE Items.project_id = ?
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Label?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var l = new Objects.Label ();

            l.item_label_id = stmt.column_int64 (0);
            l.id = stmt.column_int64 (1);
            l.name = stmt.column_text (2);
            l.color = stmt.column_int (3);
            l.project_id = stmt.column_int64 (4);

            all.add (l);
        }

        stmt.reset ();
        return all;
    }

    public bool delete_item_label (int64 id, int64 item_id, Objects.Label label) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            DELETE FROM Items_Labels WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
            stmt.reset ();
            return false;
        } else {
            item_label_deleted (id, item_id, label);
            stmt.reset ();
            return true;
        }
    }

    public bool clear_item_label (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            DELETE FROM Items_Labels WHERE item_id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
            stmt.reset ();
            return false;
        }
        
        stmt.reset ();
        return true;
    }

    public bool exists_item_label (int64 item_id, int64 label_id) {
        bool returned = false;
        Sqlite.Statement stmt;

        int res = db.prepare_v2 ("SELECT COUNT(*) FROM Items_Labels WHERE item_id = ? AND label_id = ?", -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, item_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, label_id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.ROW) {
            returned = stmt.column_int (0) <= 0;
        }
        
        stmt.reset ();
        return returned;
    }

    public int64 get_item_label (int64 item_id, int64 label_id) {
        int64 returned = 0;
        Sqlite.Statement stmt;

        int res = db.prepare_v2 ("SELECT id FROM Items_Labels WHERE item_id = ? AND label_id = ?", -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, item_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, label_id);
        assert (res == Sqlite.OK);

        if (stmt.step () == Sqlite.ROW) {
            returned = stmt.column_int64 (0);
        }
        
        stmt.reset ();
        return returned;
    }

    public void move_item_section (Objects.Item item, int64 section_id) {
        Sqlite.Statement stmt;
        string sql;
        int res;
        int64 old_section_id = item.section_id;

        sql = """
            UPDATE Items SET section_id = ?, parent_id = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, section_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, item.parent_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (3, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
        } else {
            item_section_moved (item, section_id, old_section_id);
        }

        stmt.reset ();
    }

    public void move_item_section_parent (Objects.Item item, int64 section_id, int64 parent_id) {
        Sqlite.Statement stmt;
        string sql;
        int res;
        int64 old_section_id = item.section_id;
        int64 old_parent_id = item.parent_id;

        sql = """
            UPDATE Items SET section_id = ?, parent_id = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, section_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, parent_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (3, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
        } else {
            item_section_parent_moved (item, section_id, old_section_id, parent_id, old_parent_id);
        }

        stmt.reset ();
    }

    public void move_item_parent (Objects.Item item, int64 parent_id) {
        Sqlite.Statement stmt;
        string sql;
        int res;
        int64 old_parent_id = item.parent_id;

        sql = """
            UPDATE Items SET parent_id = ? WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, parent_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, item.id);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
        } else {
            item_parent_moved (item, parent_id, old_parent_id);
        }

        stmt.reset ();
    }

    // Reminders
    public bool insert_reminder (Objects.Reminder reminder) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            INSERT OR IGNORE INTO Reminders (id, item_id, due_date)
            VALUES (?, ?, ?);
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, reminder.id);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (2, reminder.item_id);
        assert (res == Sqlite.OK);

        res = stmt.bind_text (3, reminder.due_date);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
            stmt.reset ();
            return false;
        } else {
            reminder_added (reminder);
            stmt.reset ();
            return true;
        }
    }

    public Gee.ArrayList<Objects.Reminder?> get_reminders_by_item (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, item_id, due_date FROM Reminders WHERE item_id = ? ORDER BY due_date;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Reminder?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var r = new Objects.Reminder ();

            r.id = stmt.column_int64 (0);
            r.item_id = stmt.column_int64 (1);
            r.due_date = stmt.column_text (2);

            all.add (r);
        }

        stmt.reset ();
        return all;
    }

    public Objects.Reminder? get_first_reminders_by_item (int64 id) {
        Objects.Reminder? returned = null;
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT id, item_id, due_date FROM Reminders WHERE item_id = ? ORDER BY due_date LIMIT 1;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        while (stmt.step () == Sqlite.ROW) {
            returned = new Objects.Reminder ();

            returned.id = stmt.column_int64 (0);
            returned.item_id = stmt.column_int64 (1);
            returned.due_date = stmt.column_text (2);
        }

        stmt.reset ();
        return returned;
    }

    public Gee.ArrayList<Objects.Reminder?> get_reminders () {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT Reminders.id, Reminders.item_id, Reminders.due_date, Items.content, Items.project_id FROM Reminders
            INNER JOIN Items ON Reminders.item_id = Items.id WHERE Items.checked = 0 ORDER BY Reminders.due_date;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        var all = new Gee.ArrayList<Objects.Reminder?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var r = new Objects.Reminder ();

            r.id = stmt.column_int64 (0);
            r.item_id = stmt.column_int64 (1);
            r.due_date = stmt.column_text (2);
            r.content = stmt.column_text (3);
            r.project_id = stmt.column_int64 (4);

            all.add (r);
        }

        stmt.reset ();
        return all;
    }

    public Objects.Reminder get_reminder_by_id (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            SELECT Reminders.id, Reminders.item_id, Reminders.due_date, Items.content, Items.project_id FROM Reminders
            INNER JOIN Items ON Reminders.item_id = Items.id WHERE Reminders.id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        var r = new Objects.Reminder ();
        if (stmt.step () == Sqlite.ROW) {
            r.id = stmt.column_int64 (0);
            r.item_id = stmt.column_int64 (1);
            r.due_date = stmt.column_text (2);
            r.content = stmt.column_text (3);
            r.project_id = stmt.column_int64 (4);
        }

        stmt.reset ();
        return r;
    }

    public bool delete_reminder (int64 id) {
        Sqlite.Statement stmt;
        string sql;
        int res;

        sql = """
            DELETE FROM Reminders WHERE id = ?;
        """;

        res = db.prepare_v2 (sql, -1, out stmt);
        assert (res == Sqlite.OK);

        res = stmt.bind_int64 (1, id);
        assert (res == Sqlite.OK);

        if (stmt.step () != Sqlite.DONE) {
            warning ("Error: %d: %s", db.errcode (), db.errmsg ());
            stmt.reset ();
            return false;
        } else {
            reminder_deleted (id);
            stmt.reset ();
            return true;
        }
    }

    private Gee.ArrayList<Objects.Item?> get_items_from_stmt (Sqlite.Statement stmt) {
        int res;
        var items = new Gee.ArrayList<Objects.Item?> ();

        while ((res = stmt.step ()) == Sqlite.ROW) {
            var i = create_item_from_stmt (stmt);

            items.add (i);
        }

        stmt.reset ();
        return items;
    }

    private Objects.Item create_item_from_stmt (Sqlite.Statement stmt) {
        var i = new Objects.Item ();

        i.id = stmt.column_int64 (0);
        i.project_id = stmt.column_int64 (1);
        i.section_id = stmt.column_int64 (2);
        i.user_id = stmt.column_int64 (3);
        i.assigned_by_uid = stmt.column_int64 (4);
        i.responsible_uid = stmt.column_int64 (5);
        i.sync_id = stmt.column_int64 (6);
        i.parent_id = stmt.column_int64 (7);
        i.priority = stmt.column_int (8);
        i.item_order = stmt.column_int (9);
        i.checked = stmt.column_int (10);
        i.is_deleted = stmt.column_int (11);
        i.content = stmt.column_text (12);
        i.note = stmt.column_text (13);
        i.due_date = stmt.column_text (14);
        i.due_timezone = stmt.column_text (15);
        i.due_string = stmt.column_text (16);
        i.due_lang = stmt.column_text (17);
        i.due_is_recurring = stmt.column_int (18);
        i.date_added = stmt.column_text (19);
        i.date_completed = stmt.column_text (20);
        i.date_updated = stmt.column_text (21);
        i.is_todoist = stmt.column_int (22);
        i.day_order = stmt.column_int (23);
        i.collapsed = stmt.column_int (24);

        return i;
    }
}*/
