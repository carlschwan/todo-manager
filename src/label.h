// Copyright 2019 Alain M.(https://github.com/alainm23/planner)
// Copyright 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#pragma once

#include <QObject>

/**
 * @todo write docs
 */
class Label : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isTodoist READ isTodoist WRITE setIsTodoist NOTIFY isTodoistChanged)
    Q_PROPERTY(bool isFavorite READ isFavorite WRITE setIsFavorite NOTIFY isFavoriteChanged)
    Q_PROPERTY(bool isDeleted READ isDeleted WRITE setIsDeleted NOTIFY isDeletedChanged)
    Q_PROPERTY(int itemOrder READ itemOrder WRITE setItemOrder NOTIFY itemOrderChanged)
    Q_PROPERTY(int color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(qint64 projectId READ projectId WRITE setProjectId NOTIFY projectIdChanged)
    Q_PROPERTY(qint64 itemLabelId READ itemLabelId WRITE setItemLabelId NOTIFY itemLabelIdChanged)
    Q_PROPERTY(qint64 id READ id WRITE setId NOTIFY idChanged)

public:
    /**
     * Default constructor
     */
    Label(QObject *parent = nullptr);

    /**
     * Destructor
     */
    ~Label();

    /**
     * @return the isTodoist
     */
    bool isTodoist() const;

    /**
     * @return the isFavorite
     */
    bool isFavorite() const;

    /**
     * @return the isDeleted
     */
    bool isDeleted() const;

    /**
     * @return the itemOrder
     */
    int itemOrder() const;

    /**
     * @return the color
     */
    int color() const;

    /**
     * @return the name
     */
    QString name() const;

    /**
     * @return the projectId
     */
    qint64 projectId() const;

    /**
     * @return the itemLabelId
     */
    qint64 itemLabelId() const;

    /**
     * @return the id
     */
    qint64 id() const;

public Q_SLOTS:
    /**
     * Sets the isTodoist.
     *
     * @param isTodoist the new isTodoist
     */
    void setIsTodoist(bool isTodoist);

    /**
     * Sets the isFavorite.
     *
     * @param isFavorite the new isFavorite
     */
    void setIsFavorite(bool isFavorite);

    /**
     * Sets the isDeleted.
     *
     * @param isDeleted the new isDeleted
     */
    void setIsDeleted(bool isDeleted);

    /**
     * Sets the itemOrder.
     *
     * @param itemOrder the new itemOrder
     */
    void setItemOrder(int itemOrder);

    /**
     * Sets the color.
     *
     * @param color the new color
     */
    void setColor(int color);

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    void setName(const QString& name);

    /**
     * Sets the projectId.
     *
     * @param projectId the new projectId
     */
    void setProjectId(qint64 projectId);

    /**
     * Sets the itemLabelId.
     *
     * @param itemLabelId the new itemLabelId
     */
    void setItemLabelId(qint64 itemLabelId);

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    void setId(qint64 id);

Q_SIGNALS:
    void isTodoistChanged(bool isTodoist);

    void isFavoriteChanged(bool isFavorite);

    void isDeletedChanged(bool isDeleted);

    void itemOrderChanged(int itemOrder);

    void colorChanged(int color);

    void nameChanged(const QString& name);

    void projectIdChanged(qint64 projectId);

    void itemLabelIdChanged(qint64 itemLabelId);

    void idChanged(qint64 id);

private:
    bool m_isTodoist;
    bool m_isFavorite;
    bool m_isDeleted;
    int m_itemOrder;
    int m_color;
    QString m_name;
    qint64 m_projectId;
    qint64 m_itemLabelId;
    qint64 m_id;
};
