// Copyright 2019 Alain M.(https://github.com/alainm23/planner)
// Copyright 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#include "queue.h"

Queue::Queue(QObject *parent)
    : QObject(parent)
{
}

Queue::~Queue()
{
}

QDateTime Queue::dateAdded() const
{
    return m_dateAdded;
}

void Queue::setDateAdded(QDateTime dateAdded)
{
    if (m_dateAdded == dateAdded) {
        return;
    }

    m_dateAdded = dateAdded;
    emit dateAddedChanged(m_dateAdded);
}

QString Queue::args() const
{
    return m_args;
}

void Queue::setArgs(const QString& args)
{
    if (m_args == args) {
        return;
    }

    m_args = args;
    emit argsChanged(m_args);
}

QString Queue::query() const
{
    return m_query;
}

void Queue::setQuery(const QString& query)
{
    if (m_query == query) {
        return;
    }

    m_query = query;
    emit queryChanged(m_query);
}

QString Queue::temId() const
{
    return m_temId;
}

void Queue::setTemId(const QString& temId)
{
    if (m_temId == temId) {
        return;
    }

    m_temId = temId;
    emit temIdChanged(m_temId);
}

qint64 Queue::objectId() const
{
    return m_objectId;
}

void Queue::setObjectId(qint64 objectId)
{
    if (m_objectId == objectId) {
        return;
    }

    m_objectId = objectId;
    emit objectIdChanged(m_objectId);
}

QString Queue::uuid() const
{
    return m_uuid;
}

void Queue::setUuid(const QString& uuid)
{
    if (m_uuid == uuid) {
        return;
    }

    m_uuid = uuid;
    emit uuidChanged(m_uuid);
}
