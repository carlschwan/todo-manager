// Copyright 2019 Alain M.(https://github.com/alainm23/planner)
// Copyright 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#pragma once
#include <QObject>
#include <QDateTime>

/**
 * A queue
 */
class Queue : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDateTime dateAdded READ dateAdded WRITE setDateAdded NOTIFY dateAddedChanged)
    Q_PROPERTY(QString args READ args WRITE setArgs NOTIFY argsChanged)
    Q_PROPERTY(QString query READ query WRITE setQuery NOTIFY queryChanged)
    Q_PROPERTY(QString temId READ temId WRITE setTemId NOTIFY temIdChanged)
    Q_PROPERTY(qint64 objectId READ objectId WRITE setObjectId NOTIFY objectIdChanged)
    Q_PROPERTY(QString uuid READ uuid WRITE setUuid NOTIFY uuidChanged)

public:
    Queue(QObject *parent = nullptr);
    ~Queue();

    /**
     * @return the dateAdded
     */
    QDateTime dateAdded() const;

    /**
     * @return the args
     */
    QString args() const;

    /**
     * @return the query
     */
    QString query() const;

    /**
     * @return the temId
     */
    QString temId() const;

    /**
     * @return the objectId
     */
    qint64 objectId() const;

    /**
     * @return the uuid
     */
    QString uuid() const;

public Q_SLOTS:
    /**
     * Sets the dateAdded.
     *
     * @param dateAdded the new dateAdded
     */
    void setDateAdded(QDateTime dateAdded);

    /**
     * Sets the args.
     *
     * @param args the new args
     */
    void setArgs(const QString& args);

    /**
     * Sets the query.
     *
     * @param query the new query
     */
    void setQuery(const QString& query);

    /**
     * Sets the temId.
     *
     * @param temId the new temId
     */
    void setTemId(const QString& temId);

    /**
     * Sets the objectId.
     *
     * @param objectId the new objectId
     */
    void setObjectId(qint64 objectId);

    /**
     * Sets the uuid.
     *
     * @param uuid the new uuid
     */
    void setUuid(const QString& uuid);

Q_SIGNALS:
    void dateAddedChanged(QDateTime dateAdded);

    void argsChanged(const QString& args);

    void queryChanged(const QString& query);

    void temIdChanged(const QString& temId);

    void objectIdChanged(qint64 objectId);

    void uuidChanged(const QString& uuid);

private:
    QDateTime m_dateAdded;
    QString m_args;
    QString m_query;
    QString m_temId;
    qint64 m_objectId;
    QString m_uuid;
};
