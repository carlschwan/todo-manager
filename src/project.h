// SPDX-FileCopyrightText: 2019 Alain M.(https://github.com/alainm23/planner)
// SPDX-FileCopyrightText: 2021 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-FileCopyrightText: GPL-3.0-or-later

#pragma once

#include <QObject>
#include <QDateTime>

/**
 * Project 
 */
class Project : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qint64 areaId READ areaId WRITE setAreaId NOTIFY areaIdChanged)
    Q_PROPERTY(qint64 parentId READ parentId WRITE setParentId NOTIFY parentIdChanged)
    Q_PROPERTY(qint64 id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(bool collapsed READ collapsed WRITE setCollapsed NOTIFY collapsedChanged)
    Q_PROPERTY(int sortOrder READ sortOrder WRITE setSortOrder NOTIFY sortOrderChanged)
    Q_PROPERTY(bool showCompleted READ showCompleted WRITE setShowCompleted NOTIFY showCompletedChanged)
    Q_PROPERTY(bool isKanban READ isKanban WRITE setIsKanban NOTIFY isKanbanChanged)
    Q_PROPERTY(bool shared READ shared WRITE setShared NOTIFY sharedChanged)
    Q_PROPERTY(bool isSync READ isSync WRITE setIsSync NOTIFY isSyncChanged)
    Q_PROPERTY(bool isFavorite READ isFavorite WRITE setIsFavorite NOTIFY isFavoriteChanged)
    Q_PROPERTY(bool isArchived READ isArchived WRITE setIsArchived NOTIFY isArchivedChanged)
    Q_PROPERTY(bool isDeleted READ isDeleted WRITE setIsDeleted NOTIFY isDeletedChanged)
    Q_PROPERTY(int itemOrder READ itemOrder WRITE setItemOrder NOTIFY itemOrderChanged)
    Q_PROPERTY(bool isTeamInbox READ isTeamInbox WRITE setIsTeamInbox NOTIFY isTeamInboxChanged)
    Q_PROPERTY(bool inboxProject READ inboxProject WRITE setInboxProject NOTIFY inboxProjectChanged)
    Q_PROPERTY(bool isTodoist READ isTodoist WRITE setIsTodoist NOTIFY isTodoistChanged)
    Q_PROPERTY(int color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(QDateTime dueDate READ dueDate WRITE setDueDate NOTIFY dueDateChanged)
    Q_PROPERTY(QString note READ note WRITE setNote NOTIFY noteChanged)

public:
    /**
     * Default constructor
     */
    Project(QObject *parent = nullptr);

    /**
     * Destructor
     */
    ~Project();

    /**
     * @return the collapsed
     */
    bool collapsed() const;

    /**
     * @return the sortOrder
     */
    int sortOrder() const;

    /**
     * @return the showCompleted
     */
    bool showCompleted() const;

    /**
     * @return the isKanban
     */
    bool isKanban() const;

    /**
     * @return the shared
     */
    bool shared() const;

    /**
     * @return the isSync
     */
    bool isSync() const;

    /**
     * @return the isFavorite
     */
    bool isFavorite() const;

    /**
     * @return the isArchived
     */
    bool isArchived() const;

    /**
     * @return the isDeleted
     */
    bool isDeleted() const;

    /**
     * @return the itemOrder
     */
    int itemOrder() const;

    /**
     * @return the isTeamInbox
     */
    bool isTeamInbox() const;

    /**
     * @return the inboxProject
     */
    bool inboxProject() const;

    /**
     * @return the isTodoist
     */
    bool isTodoist() const;

    /**
     * @return the color
     */
    int color() const;

    /**
     * @return the dueDate
     */
    QDateTime dueDate() const;

    /**
     * @return the note
     */
    QString note() const;

    /**
     * @return the name
     */
    QString name() const;

    /**
     * @return the id
     */
    qint64 id() const;

    /**
     * @return the parentId
     */
    qint64 parentId() const;

    /**
     * @return the areaId
     */
    qint64 areaId() const;

public Q_SLOTS:
    /**
     * Sets the collapsed.
     *
     * @param collapsed the new collapsed
     */
    void setCollapsed(bool collapsed);

    /**
     * Sets the sortOrder.
     *
     * @param sortOrder the new sortOrder
     */
    void setSortOrder(int sortOrder);

    /**
     * Sets the showCompleted.
     *
     * @param showCompleted the new showCompleted
     */
    void setShowCompleted(bool showCompleted);

    /**
     * Sets the isKanban.
     *
     * @param isKanban the new isKanban
     */
    void setIsKanban(bool isKanban);

    /**
     * Sets the shared.
     *
     * @param shared the new shared
     */
    void setShared(bool shared);

    /**
     * Sets the isSync.
     *
     * @param isSync the new isSync
     */
    void setIsSync(bool isSync);

    /**
     * Sets the isFavorite.
     *
     * @param isFavorite the new isFavorite
     */
    void setIsFavorite(bool isFavorite);

    /**
     * Sets the isArchived.
     *
     * @param isArchived the new isArchived
     */
    void setIsArchived(bool isArchived);

    /**
     * Sets the isDeleted.
     *
     * @param isDeleted the new isDeleted
     */
    void setIsDeleted(bool isDeleted);

    /**
     * Sets the itemOrder.
     *
     * @param itemOrder the new itemOrder
     */
    void setItemOrder(int itemOrder);

    /**
     * Sets the isTeamInbox.
     *
     * @param isTeamInbox the new isTeamInbox
     */
    void setIsTeamInbox(bool isTeamInbox);

    /**
     * Sets the inboxProject.
     *
     * @param inboxProject the new inboxProject
     */
    void setInboxProject(bool inboxProject);

    /**
     * Sets the isTodoist.
     *
     * @param isTodoist the new isTodoist
     */
    void setIsTodoist(bool isTodoist);

    /**
     * Sets the color.
     *
     * @param color the new color
     */
    void setColor(int color);

    /**
     * Sets the dueDate.
     *
     * @param dueDate the new dueDate
     */
    void setDueDate(const QDateTime& dueDate);

    /**
     * Sets the note.
     *
     * @param note the new note
     */
    void setNote(const QString& note);

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    void setName(const QString& name);

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    void setId(qint64 id);

    /**
     * Sets the parentId.
     *
     * @param parentId the new parentId
     */
    void setParentId(qint64 parentId);

    /**
     * Sets the areaId.
     *
     * @param areaId the new areaId
     */
    void setAreaId(qint64 areaId);

Q_SIGNALS:
    void collapsedChanged(bool collapsed);

    void sortOrderChanged(int sortOrder);

    void showCompletedChanged(bool showCompleted);

    void isKanbanChanged(bool isKanban);

    void sharedChanged(bool shared);

    void isSyncChanged(bool isSync);

    void isFavoriteChanged(bool isFavorite);

    void isArchivedChanged(bool isArchived);

    void isDeletedChanged(bool isDeleted);

    void itemOrderChanged(int itemOrder);

    void isTeamInboxChanged(bool isTeamInbox);

    void inboxProjectChanged(bool inboxProject);

    void isTodoistChanged(bool isTodoist);

    void colorChanged(int color);

    void dueDateChanged(const QDateTime& dueDate);

    void noteChanged(const QString& note);

    void nameChanged(const QString& name);

    void idChanged(qint64 id);

    void parentIdChanged(qint64 parentId);

    void areaIdChanged(qint64 areaId);

private:
    bool m_collapsed = false;
    int m_sortOrder = 0;
    bool m_showCompleted = false;
    bool m_isKanban = false;
    bool m_shared = false;
    bool m_isSync = false;
    bool m_isFavorite = false;
    bool m_isArchived = false;
    bool m_isDeleted = false;
    int m_itemOrder = 0;
    bool m_isTeamInbox = false;
    bool m_inboxProject = false;
    bool m_isTodoist = false;
    int m_color = 0;
    QDateTime m_dueDate;
    QString m_note;
    QString m_name;
    qint64 m_id = 0;
    qint64 m_parentId = 0;
    qint64 m_areaId = 0;
};
